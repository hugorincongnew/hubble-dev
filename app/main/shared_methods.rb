module ScreenMethods
  def iphone_model
    case Device.screen.height
      when 480
        "Iphone4"  
      when 568
        "Iphone5"
      when 667
        "Iphone6"     
      when 736
        "Iphone6plus"
      else
        ""
    end        
  end  
  
  def stylesheet_for(name)
     Kernel.const_get name
  end  
  
  def useLogoNavbar
    navigationController.navigationBar.setBarTintColor(Color.black)

    image_view =  UIImageView.alloc.initWithFrame(CGRectMake(20, 0, Device.screen.width - 15, navigationController.navigationBar.frame.size.height - 15))
    image_view.contentMode = UIViewContentModeScaleAspectFit
    image_view.image = UIImage.imageNamed("logo.png")

    navigationItem.titleView = image_view
    navigationController.navigationBar.translucent = false
    navigationController.navigationBar.barStyle = UIBarStyleBlack
  end
  
  def show_menu
    rmq(@menu).animate do |m|
      m.style {|st| st.top = Device.screen.height - 60}
    end
  end
  
  def hide_menu 
    rmq(@menu).animate do |m|
      m.style {|st| st.top = Device.screen.height}
    end  
  end   
  
end  

module ProMotion
  class Screen < ViewController
    include CDQ
    include ScreenMethods
    
    def will_appear
      super
      self.view.setBackgroundColor(UIColor.colorWithPatternImage(UIImage.imageNamed("bg.png")))
    end
    
    def on_init
      @current_screen = self
    end  
    
    def preferredStatusBarStyle
      UIStatusBarStyleLightContent
    end 
  end
  
  class StartScreen < Screen
    def will_appear
      super
      @top_black_view = UIView.alloc.initWithFrame([[0, 0], [Device.screen.width, 20]])
      @top_black_view.backgroundColor = Color.black
      self.view.addSubview(@top_black_view)
    end  
    
    def preferredStatusBarStyle
      UIStatusBarStyleLightContent
    end
  end  
  
  class InnerScreen < Screen
    def will_appear
      super
      useLogoNavbar
    end  
    
    def back
      close
    end      
    
    def preferredStatusBarStyle
      UIStatusBarStyleLightContent
    end 
    
  end   

  
  class TableScreen < TableViewController
    include CDQ
    include ScreenMethods
    
    def will_appear
      super
      useLogoNavbar
    end      
    
    def on_appear
      add_tab_menu_on(self.view.superview)
      @profile_nav_container_bg = rmq(self.view.superview).append(UIView, :profile_nav_container_bg).get
      pn_container = rmq(self.view.superview).append(UIView, :profile_nav_container)
      add_profile_btns(pn_container)
      @profile_nav_container = pn_container.get
      $pn_showed = false
      scroll_position > 20 ? show_menu : hide_menu
    end  
    
    def add_profile_btns(container)
      container.append(UIButton, :messages_nav_btn).data("Messages").on(:touch){|sender| go_to_messages }
      container.append(UIButton, :settings_nav_btn).data("Settings").on(:touch){|sender| go_to_settings }
      container.append(UIButton, :logout_nav_btn).data("Logout").on(:touch){|sender| sign_out }   
      container.append(UIView, :profile_nav_line1)     
      container.append(UIView, :profile_nav_line2)              
    end  
    
    def on_init
      @current_screen = self
      @current_user = CurrentUser.last.inspect
    end      
    
    def scrollViewDidScroll(scrollview)
      scroll_position > 20 ? show_menu : hide_menu
    end  
  
    def preferredStatusBarStyle
      UIStatusBarStyleLightContent
    end 
    
    private
    
    def scroll_position
      self.view.contentOffset.y
    end  
  end  
  
  class TableScreen2 < TableScreen
    include CDQ
    include ScreenMethods
    
    def will_appear
      super
      self.view.setBackgroundColor(UIColor.clearColor)
    end      
    
    def on_appear
      return nil
    end  
    
    def on_init
      @current_screen = self
      @current_user = CurrentUser.last.inspect
    end     
    
    def preferredStatusBarStyle
      UIStatusBarStyleLightContent
    end      
    
  end  
end

module ScreenSharedMethods
  
  def default_nav_items
    set_nav_bar_button :left,  { image: UIImage.imageNamed("go_back"), action: :back, accessibility_label: "go back", tint_color: Color.white }
    set_nav_bar_button :right, { image: UIImage.imageNamed("empty"), action: nil, accessibility_label: "empty", tint_color: Color.white }  
  end  
  
  def add_search_common_content
    add_search_input
    add_search_bottom_nav(2)
  end 
  
  def add_search_table(kind)
    @results_table = add_child_screen(SearchTableScreen.new(:nav_bar => false, :kind => kind))
    rmq.append(@results_table.view, :search_result_table)
  end  
  
  def add_search_input
    rmq.append(UIView, :search_input_bg)    
    @search_input = rmq.append!(UITextField, :search_input)
    @search_input.delegate = self
  end
  
  def add_search_bottom_nav(pos)
    inner_menu = rmq.append(UIView, :inner_menu_container).get
    rmq(inner_menu).append(UIView, :inner_menu_bg).get 
    rmq(inner_menu).append(UIView, "search_inner_menu_active_pos#{pos}".to_sym )
    add_inner_menu_buttons(inner_menu)
  end  
     
  def add_inner_menu_buttons(menu)
    rmq(menu).append(UIButton, :home_btn).on(:touch){|sender| close }
    rmq(menu).append(UIButton, :search_btn).on(:touch){|sender| }
    rmq(menu).append(UIButton, :photo_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_new_post_gallery
    end  
    rmq(menu).append(UIButton, :profile_btn).on(:touch) do |sender| 
       close to_screen: :root, :animated => false
       $dashboard_screen.go_to_profile
    end   
  end  
  
  def add_inner_top_bar
    @top_bar = rmq.append(UIView, :inner_top_bar)
    rmq.append(UIView, :white_line)
    add_main_action_to_bar
  end 
  
  def add_publish_action_to_bar
    @select_btn.remove
    @publish_btn = @top_bar.append(UIButton, :publish_file).on(:touch){|sender| upload_file(@file, @file_type) }
    @cancel_btn = @top_bar.append(UIButton, :cancel_file).on(:touch){|sender| cancel_file }    
  end    
  
  def add_bottom_menu(active)
    @menu = rmq(view).append(UIView, :menu_container_active).get
    rmq(@menu).append(UIView, :menu_bg_orange).get 
    rmq(@menu).append(UIView, "menu_active_pos#{active}".to_sym)
    add_bottom_buttons
  end  
  
  def add_tab_menu_on(view)
    @menu = rmq(view).append(UIView, :menu_container).get
    rmq(@menu).append(UIView, :menu_bg).get 
    add_menu_buttons(@menu)
  end     
  
  def add_bottom_buttons
    rmq(@menu).append(UIButton, :upload_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_new_post_gallery(false)
    end  
    rmq(@menu).append(UIButton, :camera_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_new_post_camera(false)
    end
    rmq(@menu).append(UIButton, :video_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_new_post_video(false)
    end
    rmq(@menu).append(UIButton, :stream_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_new_post_stream(false)
    end
  end  
  
  def add_search_top_menu(active)
    @menu = rmq(view).append(UIView, :menu_search_container_active).get
    rmq(@menu).append(UIView, :menu_search_bg_orange).get 
    rmq(@menu).append(UIView, "menu_search_active_pos#{active}".to_sym)
    add_search_buttons
  end    
  
  def add_search_buttons
    rmq(@menu).append(UIButton, :trending_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_search_trending(false)
    end  
    rmq(@menu).append(UIButton, :most_view_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_search_most_view(false)
    end
    rmq(@menu).append(UIButton, :most_like_btn).on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_to_search_most_like(false)
    end
  end   
  
  def add_menu_buttons(menu)
    rmq(menu).append(UIButton, :home_btn).on(:touch){|sender| $dashboard_screen.get_new_info}.get
    rmq(menu).append(UIButton, :search_btn).on(:touch){|sender| $dashboard_screen.go_to_search_trending}.get
    rmq(menu).append(UIButton, :photo_btn).on(:touch){|sender| $dashboard_screen.go_to_new_post_gallery}.get
    rmq(menu).append(UIButton, :profile_btn).on(:touch){|sender| $dashboard_screen.go_to_profile }.get
  end  

  def add_logo
   rmq.append(UIImageView, :logo).get
  end

  def textViewDidEndEditing(textView)
    set_view_normal(@current_screen.view)
    textView.resignFirstResponder
  end

  def textFieldShouldReturn(textField) 
    set_view_normal(@current_screen.view)
    textField.resignFirstResponder
    return false
  end    
  
  def textFieldDidBeginEditing(textField)
    top = textField.frame.origin.y.to_i
    animate_screen_for_field(top)
  end
  
  def base_input_at_top(top)
    rmq.append(UITextField, :inputs).layout(t: top + ext_top).get
  end  
  
  def add_form_line(top, width)
    v = rmq.append(UIView).get
    v.frame = [[ centered_left(width),top],[width,1]]
    v.backgroundColor = Color.main_grey
  end  
  
  def centered_left(width)
    (Device.screen.width / 2) - (width / 2)
  end    
  
  private
  
  def animate_screen_for_field(top)
    move = (top - 255) * -1
    if move < 0
      rmq(@current_screen.view).animate(
        duration: 0.3,
        animations: lambda{|v|
          v.move top: move 
        }
      )
    end  
  end  
  
  def set_view_normal(view, extra=0)
    rmq(view).animate(
      duration: 0.3,
      animations: lambda{|v|
        v.style {|st| st.top = 0 + extra}
      }
    )
  end  
  
  def ext_top
    case iphone_model
    when "Iphone4"
        -50       
      when "Iphone6"
        50  
      when "Iphone6plus"
        90
      else
        0      
    end
  end  
 
end  