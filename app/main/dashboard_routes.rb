

module DashboardRoutes
  
  def get_new_info
    self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top)
    $api.get_dashboard_info({:user_id => @current_user.id}, self)
  end  
  
  def go_to_full_view(data)
    open PostFullviewScreen.new(nav_bar: true, animated: true, post: data)
  end  
  
  def go_to_all_messages(post_id)
    open AllMessagesScreen.new(:nav_bar => true, :animated => true, :post => post_id)
  end   
  
  def go_to_public_profile(user_id)
    open PublicUserFullviewScreen.new(nav_bar: true, user: User.where(:id).eq(user_id).last)
  end    
  
  def go_to_new_post_gallery(animated=true)
    open NewPostGalleryScreen.new(nav_bar: true), {:animated => animated}
  end  
  
  def go_to_new_post_camera(animated=true)
    open NewPostCameraScreen.new(nav_bar: true), {:animated => animated}
  end    
  
  def go_to_new_post_video(animated=true)
    open NewPostVideoScreen.new(nav_bar: true), {:animated => animated}
  end   
  
  def go_to_new_post_stream(animated=true)
    open NewPostStreamScreen.new(nav_bar: true), {:animated => animated}
  end   
  
  def go_to_search_trending(animated=true)
    open SearchTrendingScreen.new(nav_bar: true), {:animated => animated}
  end  
  
  def go_to_search_most_view(animated=true)
    open SearchMostViewScreen.new(nav_bar: true,), {:animated => animated} 
  end  
  
  def go_to_search_most_like(animated=true)
    open SearchMostLikeScreen.new(nav_bar: true), {:animated => animated} 
  end    
  
  def go_to_profile(animated=true)
    open ProfileScreen.new(nav_bar: true, :user => @current_user), {:animated => animated} 
  end  
  
  def go_my_activity(animated=true)
    open ProfileActivityScreen.new(nav_bar: true, :user => @current_user), {:animated => animated} 
  end  
  
  def go_to_messages(animated=true)
    $notifier.success("This feature is under construction")
  end  

  def go_to_settings(animated=true)
    $notifier.success("This feature is under construction")
  end   
 
end  