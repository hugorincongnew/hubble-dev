
class Wizard3Screen < PM::StartScreen
  
  def on_load
    rmq.stylesheet = WizardStylesheet
    add_elements
    self.view.on_swipe :left { go_to_dashboard }
    self.view.on_swipe :right { open_wizard2 }    
  end  
  
  def add_elements
     rmq.append(UIImageView, :wizard_img_3)
     rmq.append(UIImageView, :dots_3)   
     rmq.append(UILabel, :title_line1).data("LET YOUR VOICE") 
     rmq.append(UILabel, :title_line2).data("BE SEEN") 
     rmq.append(UILabel, :text_line1).data("WHY NOT ADD A LIVE COMMENT")        
     rmq.append(UILabel, :text_line2).data("TO A LIVE STREAM!")       
  end  
  
  def go_to_dashboard
    self.navigationController.setNavigationBarHidden(false, animated:true)
    $dashboard_screen = DashboardScreen.new(nav_bar: true)
    open_root_screen $dashboard_screen
  end  
  
  def open_wizard2
    open Wizard2Screen.new(nav_bar: true), {:animated => true}
  end  
  

end  
