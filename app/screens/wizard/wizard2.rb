
class Wizard2Screen < PM::StartScreen
  
  def on_load
    rmq.stylesheet = WizardStylesheet
    add_elements
    self.view.on_swipe :left { open_wizard3 }
    self.view.on_swipe :right { open_wizard1 }    
  end  
  
  def add_elements
     rmq.append(UIImageView, :wizard_img_2)
     rmq.append(UIImageView, :dots_2)   
     rmq.append(UILabel, :title_line1).data("SNAP OR UPLOAD") 
     rmq.append(UILabel, :title_line2).data("A PICTURE") 
     rmq.append(UILabel, :text_line1).data("TO SHARE WITH YOUR FOLLOWERS")     
  end  
  
  
  def open_wizard3
    open Wizard3Screen.new(nav_bar: true), {:animated => true}
  end  
  
  def open_wizard1
    open Wizard1Screen.new(nav_bar: true), {:animated => true}
  end   
  

end  
