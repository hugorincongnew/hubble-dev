
class Wizard1Screen < PM::StartScreen
  
  def on_load
    self.navigationController.setNavigationBarHidden(true, animated:true)
    rmq.stylesheet = WizardStylesheet
    add_elements
    self.view.on_swipe :left { open_wizard2 }
  end  
  
  def add_elements
     rmq.append(UIImageView, :wizard_img_1)
     rmq.append(UIImageView, :dots_1)   
     rmq.append(UILabel, :title_line1).data("INSTANTLY") 
     rmq.append(UILabel, :title_line2).data("RECORD LIVE") 
     rmq.append(UILabel, :text_line1).data("RECORD LIVE VIDEO CONTENT TO") 
     rmq.append(UILabel, :text_line2).data("SHARE WITH YOUR FOLLOWERS")       
  end  

  def open_wizard2
    open Wizard2Screen.new(nav_bar: true), {:animated => true}
  end  
  
  def on_init
    @current_user = CurrentUser.last    
    $api.get_dashboard_info({:user_id => @current_user.id}, self)
  end  
  
  def update_info(data)
    Post.set_new(data)
  end

end  
