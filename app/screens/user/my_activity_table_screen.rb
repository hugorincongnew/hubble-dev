class MyActivityTableScreen < PM::TableScreen2  
  include ScreenSharedMethods 
  
  def on_load
    rmq.stylesheet = DashboardStylesheet
    @data = Feed.all.to_a 
    get_new_info
    update_table_data
  end
  
  def table_data
    [{ cells: data }]
  end    
  
  def data 
    @data.map do |data|
      {
        cell_class: UITableViewCellMyActivity,
        cell_identifier: "myActivity",
        title: "",
        arguments: { data: data },
        height: 60,
        style:{
          add_activity_data: data
        }
      }
    end
  end  
  
  def get_new_info
    self.tableView.contentOffset = CGPointMake(0, 0 - self.tableView.contentInset.top)
    $api.get_user_feed({:user_id => @current_user.id}, self)
  end  
  
  def update_info(data)
    @data = Feed.set_new(data)
    update_table_data
  end
  
  
  def on_init 
    @current_user = CurrentUser.last    
  end  
    
end  