
class ProfileScreen < PM::InnerScreen
  attr_accessor :user
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    @user = self.user
    rmq.stylesheet = ProfileStylesheet
    add_main_data
    add_search_bottom_nav(4)
  end  
  
  def add_main_data
    container = rmq.append(UIView, :profile_bg)
    add_user_data(container)
    add_follower_section(container)
    add_post_activity_menu(container)
    add_my_posts_table(container)
  end  
  
  def add_user_data(container)
    load_profile_image(@user.avatar, container)
    container.append(UILabel, :user_profile_name).data(@user.profile_name)
    container.append(UILabel, :profile_username).data("@#{@user.username}")    
  end  
  
  def add_follower_section(container)
    container.append(UILabel, :followers_label).data("FOLLOWERS")
    container.append(UILabel, :following_label).data("FOLLOWING")    
    
    #TODO include readl data here
    container.append(UILabel, :followers).data(@user.followers.to_s)  
    container.append(UILabel, :following).data(@user.following.to_s)    
      
    container.append(UIView, :follow_line)          
  end  
  
  def add_post_activity_menu(container)
    container.append(UIView, :orange_line) 
    container.append(UIView, :post_active_line) 
    container.append(UILabel, :my_posts_active).data("MY POSTS")
    container.append(UIButton, :activity).data("ACTIVITY").on(:touch) do |sender| 
      close to_screen: :root, :animated => false
      $dashboard_screen.go_my_activity(false)
    end     
  end  
  
  def add_my_posts_table(container)
    #TODO get my posts real data
    my_posts = add_child_screen(MyPostsTableScreen.new(:nav_bar => false, :posts => Post.all))
    container.append(my_posts.view, :my_posts_table)
  end  
  
  def on_init 
    default_nav_items     
    @current_screen = self
  end

end  
