class MyPostsTableScreen < PM::TableScreen2  
  attr_accessor :posts 
  include ScreenSharedMethods
    
  def on_init
    rmq.stylesheet = DashboardStylesheet
    @data = posts
    update_table_data
  end
  
  def table_data
    [{ cells: data }]
  end    
  
  def data 
    @data.map do |data|
      {
        cell_class: UITableViewCellMyPost,
        cell_identifier: "myPost#{data.id}",
        title: "",
        action: :go_to_full_view,
        arguments: { data: data },
        height: (Device.screen.height * 0.7),
        style:{
          add_main_image_post: data
        }
      }
    end
  end  
  
  def go_to_full_view(data)
    $dashboard_screen.go_to_full_view(data)
  end  
    
end  