class DashboardScreen < PM::TableScreen
  include ScreenSharedMethods 
  include DashboardRoutes
  longpressable
  refreshable
  
  def on_load
    useLogoNavbar
    get_new_info
    @data = Post.all.to_a
    update_table_data  
    $api.update_follows
    
  end  
  
  def on_refresh
   get_new_info
  end
  
  def will_appear
    BW::Location.get_once(purpose: 'We need to use your location to position itself in the world.') do |result|
      if result.is_a?(CLLocation)
        $api.get_location_in(result.coordinate.latitude, result.coordinate.longitude)
      else
        $current_location = "unknow"
        NSLog "IOS LOCATION ERROR: #{result[:error]}"
      end
    end
    get_new_info
  end  
  
  def table_data
    [{ cells: data }]
  end  

  def data 
    @data.map do |data|
      {
        cell_class: UITableViewCellDashboard,
        cell_identifier: "DashboardItem#{data.id}",
        title: "",
        #action: :go_to_full_view,
        #long_press_action: :like_this_post,
        arguments: { data: data },
        height: (Device.screen.height - 60),
        style:{
          add_main_data: data
        }
      }
    end
  end  
  
  def like_this_post(data)
    $api.set_new_like({:like => {:user_id => @current_user.id, :post_id => data[:data].id}}, self)
    $notifier.success("You like this post")
  end  
  
  def update_info(data)
    @data = Post.set_new(data)
    stop_refreshing
    update_table_data
  end
  
  def on_init 
    @current_user = CurrentUser.last   
    config_pn
  end
  
  def config_pn
    user_img_btn = rmq.append(UIImageView, :profile_container_nav).layout(:left => 0, :top => 0, :width => 30, :height => 30).style do |st| 
      st.corner_radius = 15
      st.masks_to_bounds = true
      st.view.url = {url: @current_user.avatar, placeholder: UIImage.imageNamed("icon-profile.png")}
      st.view.userInteractionEnabled = true
    end
    
    view_item = user_img_btn.get
    view_item.on_tap{|sender| show_hide_profile_nav }
    set_nav_bar_button :right,  { custom_view: view_item, accessibility_label: "pn" }
    set_nav_bar_button :left, { image: UIImage.imageNamed("empty"), action: nil, accessibility_label: "empty", tint_color: Color.white }
  end  
  
  def show_hide_profile_nav
    if $pn_showed 
      hide_pn
      $pn_showed = false
    else
      show_pn
      $pn_showed = true
    end    
  end  
  
  def show_pn
    @profile_nav_container.slide :down, 150
    @profile_nav_container_bg.slide :down, 151
  end  
  
  def hide_pn
    @profile_nav_container.slide :up, 150
    @profile_nav_container_bg.slide :up, 151
  end  
  
  def will_appear
    rmq.stylesheet = DashboardStylesheet
    $api.update_current_user
  end  
  
  def sign_out
    $api.sign_out(self)
  end  
  
  def close_session
    CurrentUser.destroy_all
    open_modal LoginScreen.new(nav_bar: false, animated: true)
  end  
  
end  
