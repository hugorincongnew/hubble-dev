class PublicUserFullviewScreen < PM::InnerScreen
  attr_accessor :user
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    @user = self.user
    $api.update_user_info(@user.id, self)
    rmq.stylesheet = ProfileStylesheet
    add_public_main_data
    add_search_bottom_nav(4)
  end  
  
  def add_public_main_data
    @container = rmq.append(UIView, :profile_bg)
    add_user_data
    add_follower_section_public
    add_my_posts_table
    add_follow_btn
  end  
  
  def add_user_data
    load_profile_image(@user.avatar, @container)
    @container.append(UILabel, :user_profile_name).data(@user.profile_name)
    @container.append(UILabel, :profile_username).data("@#{@user.username}")    
  end  
  
  def add_follower_section_public
    @container.append(UILabel, :followers_label_public).data("FOLLOWERS")
    @container.append(UILabel, :following_label_public).data("FOLLOWING")    
    
    #TODO include readl data here
    @container.append(UILabel, :followers_public).data(@user.followers.to_s)  
    @container.append(UILabel, :following_public).data(@user.following.to_s)    
      
    @container.append(UIView, :follow_line_public)          
  end  
  
  def add_post_activity_menu
    @container.append(UIView, :orange_line) 
    @container.append(UIView, :post_active_line) 
    @container.append(UILabel, :my_posts_active).data("MY POSTS")
    @container.append(UIButton, :activity).data("ACTIVITY")   
  end  
  
  def add_my_posts_table
    #TODO get my posts real data
    my_posts = add_child_screen(MyPostsTableScreen.new(:nav_bar => false, :posts => Post.all))
    @container.append(my_posts.view, :my_posts_table).layout(:top => 105)
  end  
  
  def add_follow_btn
    if @user.following_user?
      unfollow_btn
    else
      follow_btn
    end    
  end  
  
  def unfollow_btn
    @unfollow = @container.append(UIButton, :unfollow_btn).on(:touch){|sender| $api.unfollow(@user, self) }
  end  
  
  def follow_btn
    @follow = @container.append(UIButton, :follow_btn).on(:touch){|sender| $api.follow(@user, self) }
  end  
  
  def follow_unfollow_response(data)
    $api.update_user_info(data["user"]["id"], self)
    if data["info"] == "User followed"
      User.add_or_update_follow(data["user"])
      show_unfollow_btn
    else
      User.unfollow(data["user"]["id"])
      show_follow_btn
    end    
  end  
  
  def show_unfollow_btn
    @follow.remove
    @unfollow = unfollow_btn
  end  
  
  def show_follow_btn
    @unfollow.remove
    @follow = follow_btn
  end  

  def on_init 
    @current_user = CurrentUser.get   
    default_nav_items  
  end  
  
  def refresh_info
    @user = User.where(:id).eq(@user.id).last
    @container.remove
    rmq.stylesheet = ProfileStylesheet
    add_public_main_data
  end  

end  
