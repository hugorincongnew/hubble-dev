
module CommonPostMethods
  include ImageHelper
  
  def add_title(title)
    title_container = rmq.append(UIView, :post_title_bg).get
    rmq(title_container).append(UILabel, :post_title).data(title)
  end  

  def main_image(url)
    load_fullview_image(url)
  end  
  
  def main_video(url)
    BW::Media.play(url, {:scaling_mode => MPMovieScalingModeAspectFill, :control_style => MPMovieControlStyleNone, :movie_source_type => MPMovieSourceTypeStreaming}) do |media_player|
      media_player.view.frame = [[0, 0], [Device.screen.width, Device.screen.height * 0.7]]
      rmq.append(media_player.view)
    end
  end  
  
  def add_main_data=(data)
    
    self.on_tap(2) do
      $dashboard_screen.like_this_post({:data => data})
    end  
    
    self.on_swipe :left do
      $dashboard_screen.go_to_full_view({:data => data})
    end  
    
    self.subviews.each{|s| s.removeFromSuperview}
    data.file_type == 0 ? main_image(data.file) : main_video(data.file)
    add_user_info(data.get_user)
    add_post_info(data)
    rmq.append(UIView, :bottom_line)
  end  
  
  def add_user_info(user)
    add_profile_image(user.avatar)
    add_username(user)
  end  
  
  def add_post_info(data)
    add_location_info(data.location)
    add_views_info(data.views)
    add_likes_info(data.likes, data.like)
    add_comments_info(data.comments.count, data.id)        
  end
  
  def add_location_info(val)
    rmq.append(UIImageView, :location_icon)
    append_label_for(:location, val)
  end  
  
  def add_views_info(val)
    rmq.append(UIImageView, :views_icon)
    append_label_for(:views, val)
  end
  
  def add_likes_info(val, like=false)
    rmq.append(UIImageView, like == "true" ? :likes_icon_red : :likes_icon )
    append_label_for(:likes, val)
  end
  
  def add_comments_info(val, post_id)
    rmq.append(UIButton, :comments_icon).on(:touch){|sender| $dashboard_screen.go_to_all_messages(post_id) }
    append_label_for(:comments, val)
  end  
  
  def append_label_for(tag, value)
    rmq.append(UILabel, tag).data(value.to_s)
  end  
  
  def add_username(user)
    rmq.append(UIButton, :username).data("@#{user.username.downcase}").on(:tap, taps_required: 1){|sender| $dashboard_screen.go_to_public_profile(user.id) }
  end   
  
  def add_profile_image(url)
    load_dashboard_profile_image(url)
  end    
  
  def table_height(messages)
    count = messages.count > 5 ? 5 : messages.count - 1
    ((count + 1) * 55)
  end  
   
end  