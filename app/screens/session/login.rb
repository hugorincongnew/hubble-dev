
class LoginScreen < PM::StartScreen
  include ScreenSharedMethods
  
  def on_load
    rmq.stylesheet = stylesheet_for("BaseStylesheet#{iphone_model}")
    add_logo
    add_login_form
    check_for_loged_user
  end 
  
  def on_init
    @current_screen = self
  end  
  
  def check_for_loged_user
    unless CurrentUser.get.nil?
      $dashboard_screen = DashboardScreen.new(nav_bar: true)
      open_root_screen $dashboard_screen
    end  
  end  
  
  def add_login_form  
    add_email_field(196 )
    add_password_field(276)
    add_create_account_btn
    add_signin_btn
    add_forgot_btn
  end
  
  def add_email_field(top)
    #email input
    @email_field = base_input_at_top(top)
    @email_field.setDelegate(self)
    @email_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('YOUR EMAIL', attributes: { NSForegroundColorAttributeName => Color.main_grey})
    rmq.append(UIImageView, :email_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end  

  def add_password_field(top)
    #password input
    @password_field = base_input_at_top(top)
    @password_field.setDelegate(self)
    @password_field.secureTextEntry = true
    @password_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('PASSWORD', attributes: { NSForegroundColorAttributeName => Color.main_grey})
    rmq.append(UIImageView, :password_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end 
  
  def add_create_account_btn
    rmq.append(UIButton, :new_account_btn).data("CREATE AN ACCOUNT").layout(t: (395 + ext_top)).on(:touch){|sender| open_modal RegisterScreen.new(nav_bar: false, animated: true)}.get
    rmq.append(UIView, :create_account_line).layout(t: (ext_top + 424)).get
  end    
  
  def add_signin_btn
    rmq.append(UIButton, :red_btn).data("SIGN IN").on(:touch){|sender| signin }.get
  end    
  
  def add_forgot_btn
    rmq.append(UIButton, :forgot_btn).data("FORGOT?").layout(t: (276+ ext_top)).on(:touch){|sender| open_modal ForgotPasswordScreen.new(nav_bar: false, animated: true)}.get
  end  
    
  def open_dashboard
    $dashboard_screen = DashboardScreen.new(nav_bar: true)
    open_root_screen $dashboard_screen
  end    

  private 
  
  def signin
    $api.sign_in({user: {email: @email_field.text.downcase, password: @password_field.text}}, self)  
  end  

end  
