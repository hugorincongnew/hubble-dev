class ForgotPasswordScreen < PM::StartScreen
  include ScreenSharedMethods
  
  def on_load
    rmq.stylesheet = stylesheet_for("BaseStylesheet#{iphone_model}")
    add_logo
    add_form
  end  
  
  def add_form
    add_label_title(210)
    add_email_field(365)
    add_submit_btn
    add_back_btn
  end  
  
  def add_label_title(top)
    rmq.append(UILabel, :forgot_password_title).layout(t: (top + ext_top)).get
  end  
  
  def add_email_field(top)
    @email_field = base_input_at_top(top)
    @email_field.setDelegate(self)
    @email_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('ENTER YOUR EMAIL ADDRESS', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :email_icon).layout(t: ((top + ext_top) + 10) ).get

    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end    
  
  def add_submit_btn
    rmq.append(UIButton, :red_btn).data("SUBMIT").on(:touch){|sender| submit }.get
  end      
  
  def submit
    $temp_email = @email_field.text.downcase
    $api.forgot_password({user: {email: $temp_email}}, self)
  end  
  
  def add_back_btn
    button_t = (iphone_model == "Iphone4" ? 30 : 0)
    rmq.append(UIButton, :back_btn).data("GO BACK TO LOGIN SCREEN").layout(t: (100 + ext_top + button_t) ).on(:touch){|sender| open_modal LoginScreen.new(nav_bar: false, animated: true)}.get 
    rmq.append(UIView, :back_to_login_line).layout(t: (129 + ext_top + button_t)).get
  end    
  
  def go_to_code
    open CodePasswordScreen.new(nav_bar: false)
  end  
  
end  
