class ResetPasswordScreen < PM::StartScreen
  include ScreenSharedMethods
  
  def on_load
    rmq.stylesheet = stylesheet_for("BaseStylesheet#{iphone_model}")
    add_logo
    add_form
  end  
  
  def add_form
    add_label_title(210)
    add_pass_field(296)   
    add_confirm_pass_field(365)
    add_submit_btn
    add_back_btn
  end  
  
  def add_label_title(top)
    rmq.append(UILabel, :reset_password_title).layout(t: (top + ext_top)).get
  end  
  
  def add_pass_field(top)
    @password_field = base_input_at_top(top)
    @password_field.setDelegate(self)
    @password_field.secureTextEntry = true
    @password_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('ENTER YOUR NEW PASSWORD', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :password_icon).layout(t: ((top + ext_top) + 10) ).get

    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end     
  
  def add_confirm_pass_field(top)
    @password_confirmation_field = base_input_at_top(top)
    @password_confirmation_field.setDelegate(self)
    @password_confirmation_field.secureTextEntry = true
    @password_confirmation_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('CONFIRM YOUR NEW PASSWORD', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :password_icon).layout(t: ((top + ext_top) + 10) ).get

    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end    
  
  def add_submit_btn
    rmq.append(UIButton, :red_btn).data("SUBMIT").on(:touch){|sender| submit }.get
  end      
  
  def submit
    $api.reset_password({user: {email: $temp_email, password: @password_field.text, password_confirmation: @password_confirmation_field.text}}, self)
  end  
  
  def add_back_btn
    button_t = (iphone_model == "Iphone4" ? 30 : 0)        
    rmq.append(UIButton, :back_btn).data("GO BACK TO LOGIN SCREEN").layout(t: (100 + ext_top + button_t) ).on(:touch){|sender| go_to_login}.get 
    rmq.append(UIView, :back_to_login_line).layout(t: (129 + ext_top + button_t)).get
  end    
  
  def go_to_login
    open LoginScreen.new(nav_bar: false)
  end  
  
end  
