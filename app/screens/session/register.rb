class RegisterScreen < PM::StartScreen
  include ScreenSharedMethods  
  
  def on_load
    rmq.stylesheet = stylesheet_for("BaseStylesheet#{iphone_model}")
    add_logo
    add_back_btn
    add_signup_form   
  end  
  
  def add_back_btn
    button_t = (iphone_model == "Iphone4" ? 20 : 0)
    rmq.append(UIButton, :back_btn).data("I HAVE A HUBBLE ACCOUNT").layout(t: (110 + ext_top + button_t) ).on(:touch){|sender| open LoginScreen.new(nav_bar: false)}.get 
    rmq.append(UIView, :back_to_login_line).layout(t: (139 + ext_top + button_t)).get
  end  
  
  def add_signup_form
    add_full_name_field(158)
    add_username_field(227)
    add_email_field(296)
    add_password_field(365)
    add_confirmation_password_field(434)    
    add_register_btn
  end  
  
  def add_full_name_field(top)
    @full_name_field = base_input_at_top(top)
    @full_name_field.setDelegate(self)
    @full_name_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('FULL NAME', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :user_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 50) , Device.screen.width * 0.78)
  end
  
  def add_username_field(top)
    @username_field = base_input_at_top(top)
    @username_field.setDelegate(self)
    @username_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('@USERNAME', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :user_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 50) , Device.screen.width * 0.78)
  end
  
  def add_email_field(top)
    @email_field = base_input_at_top(top)
    @email_field.setDelegate(self)
    @email_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('YOUR EMAIL', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :email_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end
  
  def add_password_field(top)
    @password_field = base_input_at_top(top)
    @password_field.setDelegate(self)
    @password_field.secureTextEntry = true
    @password_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('PASSWORD', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :password_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end        
  
  def add_confirmation_password_field(top)
    @password_confirmation_field = base_input_at_top(top)
    @password_confirmation_field.setDelegate(self)
    @password_confirmation_field.secureTextEntry = true
    @password_confirmation_field.attributedPlaceholder = NSAttributedString.alloc.initWithString('CONFIRM YOUR PASSWORD', attributes: { NSForegroundColorAttributeName => Color.main_grey})

    rmq.append(UIImageView, :password_icon).layout(t: ((top + ext_top) + 10) ).get

    #line
    add_form_line(((top + ext_top) + 49) , Device.screen.width * 0.78)
  end  
  
  def add_register_btn
    rmq.append(UIButton, :red_btn).data("REGISTER").on(:touch){|sender| register }.get
  end  
  
  def open_dashboard
    open Wizard1Screen.new(nav_bar: true, animated: true)
  end     
  
  private

  def register
    $api.register({user: {email: @email_field.text, password: @password_field.text, :password_confirmation => @password_confirmation_field.text, :full_name => @full_name_field.text, :username => @username_field.text}}, self)    
  end  

end  