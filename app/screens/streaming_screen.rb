class StreamingScreen < PM::Screen
 
  def on_load
    setupCameraView
    setupRecordButton
    rmq.stylesheet = StreamingStylesheet
  end  
 
  def on_init
    @recorder = KFRecorder.alloc.init
    @recorder.delegate = self
  end  
  
  def setupRecordButton
    @recordButton = rmq.append(UIButton, :record_button).on(:touch){|sender| recordButtonPressed}
    
  end  
  
  def setupCameraView
    @preview = @recorder.previewLayer
    @preview.removeFromSuperlayer
    @preview.frame = CGRectMake(0, 0, Device.screen.width, Device.screen.height - 130)
    @preview.connection.setVideoOrientation(AVCaptureVideoOrientationPortrait)
    self.view.layer.addSublayer(@preview)
  end  
  
  def recordButtonPressed
    
  end  
  
  private
  
  def orientation
    UIApplication.sharedApplication.statusBarOrientation
  end  
 
end