class UITableViewCellMyActivity < PM::TableViewCell
  include ImageHelper
  include CommonPostMethods
  
  def initWithStyle(style_name, reuseIdentifier: reuseIdentifier)
    super.tap do
      rmq.stylesheet = DashboardStylesheet
      stylish
    end
  end
  
  def stylish
    self.backgroundColor = nil
    self.selectionStyle = UITableViewCellSelectionStyleNone
    self.textLabel.textColor = Color.black
    self.textLabel.font = UIFont.fontWithName("Trebuchet MS", size:18.0)
  end  
  
  def add_activity_data=(data)
    rmq.append(UIView, :bottom_line)
    @data = data
    @user = @data.user
    @post = @data.post
    self.send("#{data.feed_type}_style")
    self.on_swipe :left do
      $dashboard_screen. go_to_full_view({:data => @post})
    end  
  end    
  
  def like_style
    common_items
    rmq.append(UILabel, :like_comment_label).data("liked this #{@post.file_type == 0 ? 'picture' : 'video'}:")    
  end  
  
  def comment_style
    common_items
    rmq.append(UILabel, :like_comment_label).data("commented this #{@post.file_type == 0 ? 'picture' : 'video'}:")
  end   
  
  def common_items
    micro_profile_image(@user.avatar, :activity_profile_image, self)
    rmq.append(UIButton, :activity_username).data("#{@user.username}").on(:tap, taps_required: 1){|sender| $dashboard_screen.go_to_public_profile(@user.id) }
    item_preview
  end  
  
  def item_preview
    if @post.file_type == 0
      load_activity_image(@post.file)
    else
      load_activity_image(nil)
    end 
  end  

end  