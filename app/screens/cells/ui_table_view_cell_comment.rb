class UITableViewCellComment < PM::TableViewCell
  include ImageHelper
  
  def initWithStyle(style_name, reuseIdentifier: reuseIdentifier)
    super.tap do
      rmq.stylesheet = CommentsStylesheet
      stylish
    end
  end
  
  def stylish
    self.setBackgroundColor(UIColor.clearColor)
    self.selectionStyle = UITableViewCellSelectionStyleNone
  end  
  
  def add_data=(data)
    #TODO get user info
    self.subviews.each{|s| s.removeFromSuperview}
    mb = rmq.append(UIView, :main_bg)
    container = mb.append(UIView, :message_container)
    mini_profile_image(nil, :messages_profile_image, container)
    container.append(UILabel, post_owner(data) ? :owner_username : :username).data("@#{data[0][:username]}")
    container.append(UILabel, :comment).data(data[0][:message])
  end  
  
  def post_owner(data)
    data[1].user_id.to_i == data[0][:user_id].to_i unless data[1].nil?
  end
  
end  