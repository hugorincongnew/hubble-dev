class UITableViewCellMyPost< PM::TableViewCell
  include ImageHelper
  include CommonPostMethods
  
  def initWithStyle(style_name, reuseIdentifier: reuseIdentifier)
    super.tap do
      rmq.stylesheet = DashboardStylesheet
      stylish
    end
  end
  
  def stylish
    self.backgroundColor = Color.black
    self.selectionStyle = UITableViewCellSelectionStyleNone
    self.textLabel.textColor = Color.black
    self.textLabel.font = UIFont.fontWithName("Trebuchet MS", size:18.0)
  end  
  
  def add_main_image_post=(data)
    self.subviews.each{|s| s.removeFromSuperview}
    data.file_type == 0 ? main_image(data.file) : main_video(data.file)
    rmq.append(UIView, :bottom_line)
  end    

end  