class UITableViewCellDashboard < PM::TableViewCell
  include ImageHelper
  include CommonPostMethods
  
  def initWithStyle(style_name, reuseIdentifier: reuseIdentifier)
    super.tap do
      rmq.stylesheet = DashboardStylesheet
      stylish
    end
  end
  
  def stylish
    self.backgroundColor = Color.black
    self.selectionStyle = UITableViewCellSelectionStyleNone
    self.textLabel.textColor = Color.black
    self.textLabel.font = UIFont.fontWithName("Trebuchet MS", size:18.0)
  end  

end  