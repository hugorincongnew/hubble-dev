class SearchTableScreen < PM::TableScreen2  
  attr_accessor :kind  
  include ScreenSharedMethods
    
  def on_init
    rmq.stylesheet = DashboardStylesheet
    @data = default_data
    new_data
    update_table_data
  end
  
  def table_data
    [{ cells: data }]
  end    
  
  def reload_all
    new_data
  end  
  
  def data 
    @data.map do |data|
      {
        cell_class: UITableViewCellDashboard,
        cell_identifier: "SearchItem#{data.id}",
        title: "",
        action: :go_to_full_view,
        long_press_action: :like_this_post,
        arguments: { data: data },
        height: (Device.screen.height * 0.7),
        style:{
          add_main_data: data
        }
      }
    end
  end  
  
  def new_data
    self.send("get_new_#{self.kind}_data")
  end  
  
  def default_data
    data = get_object(self.kind).all
    data.count == 0 ? Post.all : data
  end  
  
  def get_object(string)
    Object.const_get string.split("_").map{|w| w.capitalize}.join
  end  
  
  def go_to_full_view(data)
    $dashboard_screen.go_to_full_view(data)
  end  
  
  def like_this_post(data)
    $api.set_new_like({:like => {:user_id => @current_user.id, :post_id => data[:data].id}}, self)
    $notifier.success("You like this post")
    new_data
  end  
  
  def update_info(data)
    puts data.inspect
    @data = get_object(self.kind).set_new(data)
    update_table_data
  end  
  
  def get_new_trending_data
    $api.get_trending(self)
  end
  
  def get_new_most_view_data
    $api.get_most_view(self)
  end  
  
  def get_new_most_like_data
    $api.get_most_like(self)
  end  
    
end  