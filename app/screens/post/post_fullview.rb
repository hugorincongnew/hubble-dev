
class PostFullviewScreen < PM::InnerScreen
  attr_accessor :post
  include ScreenSharedMethods
  include ImageHelper
  include CommonPostMethods
  
  def on_load
    @post = Post.find_or_create(self.post[:data])
    @post_id = @post.id
    rmq.stylesheet = PostStylesheet
    add_post_data(@post)
    @comment_section = rmq.append(UIView, :comment_section).get
    @loading_comments = loading_comments_image(@comment_section)
    $api.set_new_view({:view => {:user_id => @current_user.id, :post_id => @post_id}})
  end  
  
  def main_video(url)
    BW::Media.play(url, {:scaling_mode => MPMovieScalingModeAspectFill, :control_style => MPMovieControlStyleNone, :movie_source_type => MPMovieSourceTypeStreaming}) do |media_player|
      media_player.view.frame = [[0, 0], [Device.screen.width, Device.screen.height]]
      rmq.append(media_player.view).on(:tap){|sender| media_player.play }
    end
  end  
  
  def on_appear
    update_comments
  end  
  
  def update_comments
    @messages_container.remove if @messages_container
    $api.get_post_comments(@post_id, self)
  end  
  
  def add_post_data(data)
    data.file_type == 0 ? main_image(data.file) : main_video(data.file)
    add_user_info(data.get_user)
    add_post_info(data)
    add_title("#{data.title} - #{data.location}")
    rmq.append(UIView, :top_line)
  end  
  
  def add_messages(all_messages)
    #TEMP DATA
    @loading_comments.remove
    all_messages = formated_comments(all_messages)
    messages = all_messages.first(3)
    rmq.append(UIButton, :all_messages).data(messages.count >= 3 ? "VIEW ALL COMMENTS" : "WRITE A COMMENT" ).layout(:top => (Device.screen.height - 60) - (table_height(messages) + 100)).on(:touch){|sender| open AllMessagesScreen.new(:nav_bar => true, :animated => true, :post => @post_id) } 

    if all_messages.count > 0
      
      messages_table = add_child_screen(MessagesTableScreen.new(:nav_bar => false, :messages => messages))
      @messages_container = rmq.append(messages_table.view, :messages_table).layout(:top => (Device.screen.height - 60) - (table_height(messages) + 65), :height => table_height(messages))
    end  
  end  
  
  def formated_comments(all_messages)
    all_messages.map{|m| {:id => m["id"], :message => m["description"], :user_id => m["user"]["id"], :username => m["user"]["username"], :avatar => m["user"]["avatar"]}}
  end  
  
  def on_init 
    default_nav_items  
    @current_user = CurrentUser.last    
  end
  

end  
