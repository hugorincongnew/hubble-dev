class NewPostVideoScreen < PM::InnerScreen
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    rmq.stylesheet = PostStylesheet
    rmq.append(UIButton, :take_video_image).on(:touch){|sender| take_video } 
    add_bottom_menu(3)
    add_inner_top_bar
  end  
  
  def add_main_action_to_bar
    @select_btn = @top_bar.append(UIButton, :select_from_video).on(:touch){|sender| take_video }
  end    
  
  def cancel_file
    add_main_action_to_bar
    remove_player  
    @publish_btn.remove
    @cancel_btn.remove
    @file = nil
  end    
  
  def take_video
    BW::Device.camera.rear.picture(media_types: [:movie], video_quality: :low, video_maximum_duration: 60) do |result|
      if result[:media_type]
        data = NSData.dataWithContentsOfURL(result[:media_url])
        @file_url = result[:media_url]
        @file = data.base64Encoding
        add_publish_action_to_bar     
        set_container_image
      end  
    end
  end  
  
  def remove_player
    if @player_view
      @player_view.remove
      @player_view = nil
    end 
  end 
  
  def set_container_image   
    BW::Media.play(@file_url) do |media_player|
      media_player.view.frame = [[0, 45], [Device.screen.width, Device.screen.height - 170]]
      @player_view = rmq.append(media_player.view)
    end
  end     
  
  def upload_file(file, type)
    base64 = "data:video/m4v;base64, #{file}"
    data = {:post => { :user_id => @current_user.id, :description => "default", :location => $current_location, :views => 0, :post_type_id => "1", :file => base64 }}
    $api.new_post(data, self) 
  end    
  
  def on_init 
    default_nav_items      
    @current_screen = self
    @current_user = CurrentUser.last
  end
  
end  
