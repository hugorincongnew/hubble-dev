class NewPostStreamScreen < PM::InnerScreen
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    rmq.stylesheet = PostStylesheet
    add_bottom_menu(4)
    add_streaming_screen
  end  
  
  def on_init 
    default_nav_items   
  end
  
  def add_streaming_screen
    child_screen = add_child_screen(StreamingScreen.new(nav_bar: false))
    @stream_container = rmq.append(child_screen.view, :streaming_container)
  end  
  
end