class NewPostGalleryScreen < PM::InnerScreen
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    rmq.stylesheet = PostStylesheet
    rmq.append(UIButton, :select_from_library_image).on(:touch){|sender| select_from_library }
    @image_container = rmq.append(UIImageView, :main_image_container)    
    add_bottom_menu(1)
    add_inner_top_bar
  end   
  
  def cancel_file
    add_main_action_to_bar
    remove_player
    @publish_btn.remove
    @cancel_btn.remove
    @file = nil
    @file_type = nil
  end  
  
  def add_main_action_to_bar
    @select_btn = @top_bar.append(UIButton, :select_from_library).on(:touch){|sender| select_from_library }
  end    
  
  def select_from_library
    BW::Device.camera.any.picture(media_types: [:movie, :image]) do |result|
      if result[:media_type]
        if result[:media_type] == :image
          process_image(result)
        else
          process_movie(result)
        end    
      end
    end
  end  
  
  def process_image(result)
    @file_type = :image
    @file = result[:original_image].scale_to_fill([414, 736])
    add_publish_action_to_bar
    set_container_image
  end
  
  def process_movie(result)
    @file_type = :movie
    data = NSData.dataWithContentsOfURL(result[:media_url])
    @file_url = result[:media_url]
    @file = data.base64Encoding
    add_publish_action_to_bar     
    set_container_image
  end    
  
  def set_container_image 
    remove_player
    if @file_type == :movie
      BW::Media.play(@file_url) do |media_player|
        media_player.view.frame = [[0, 45], [Device.screen.width, Device.screen.height - 170]]
        @player_view = rmq.append(media_player.view)
      end
    else  
      @image_container.style{|st| st.image = @file}
    end
  end  
  
  def remove_player
    @image_container.style{|st| st.image = nil}
    if @player_view
      @player_view.remove
      @player_view = nil
    end 
  end  
  
  def upload_file(file, type)
    if type == :image
      image = UIImageJPEGRepresentation(file, 0.8)
      base64 = "data:image/jpg;base64, #{[image].pack("m")}"
      data = {:post => { :user_id => @current_user.id, :description => "default", :location => $current_location, :views => 1, :post_type_id => "0", :file => base64 }}
      $api.new_post(data, self)
    else
      base64 = "data:video/m4v;base64, #{file}"
      data = {:post => { :user_id => @current_user.id, :description => "default", :location => $current_location, :views => 0, :post_type_id => "1", :file => base64 }}
      $api.new_post(data, self) 
    end    
  end  
  
  def on_init 
    default_nav_items  
    @current_screen = self
    @current_user = CurrentUser.last
  end
  
end  
