class NewPostCameraScreen < PM::InnerScreen
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    rmq.stylesheet = PostStylesheet
    rmq.append(UIButton, :take_photo_image).on(:touch){|sender| take_photo }
    @image_container = rmq.append(UIImageView, :main_image_container)    
    add_bottom_menu(2)
    add_inner_top_bar
  end  
  
  def add_main_action_to_bar
    @select_btn = @top_bar.append(UIButton, :select_from_photo).on(:touch){|sender| take_photo }
  end    
  
  def cancel_file
    add_main_action_to_bar
    @publish_btn.remove
    @cancel_btn.remove
    @file = nil
    @image_container.style{|st| st.image = nil}
  end    
  
  def take_photo
    BW::Device.camera.rear.picture(media_types: [:image]) do |result|
      if result[:media_type]
        @file = result[:original_image].scale_to_fill([414, 736])
        add_publish_action_to_bar
        set_container_image 
      end   
    end
  end  
  
  def set_container_image   
    @image_container.style{|st| st.image = @file}
  end     
  
  def upload_file(file, type)
    image = UIImageJPEGRepresentation(file, 0.8)
    base64 = "data:image/jpg;base64, #{[image].pack("m")}"
    data = {:post => { :user_id => @current_user.id, :description => "default", :location => $current_location, :views => 1, :post_type_id => "0", :file => base64 }}
    $api.new_post(data, self)
  end    
  
  def on_init 
    default_nav_items  
    @current_screen = self
    @current_user = CurrentUser.last
  end
  
end  
