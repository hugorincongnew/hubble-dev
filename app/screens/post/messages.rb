class MessagesTableScreen < PM::TableScreen2  
  attr_accessor :messages  
  attr_accessor :post  
  include ScreenSharedMethods
    
  def on_init
    rmq.stylesheet = CommentsStylesheet
    @post = self.post
    @data = messages
    update_table_data
  end
  
  def table_data
    [{ cells: data }]
  end    
  
  def data 
    @data.map do |data|
      {
        cell_class: UITableViewCellComment,
        cell_identifier: "CommentItem#{data[:id]}",
        title: "",
        arguments: { data: data },
        height: 55,
        style:{
          add_data: [data, @post]
        }
      }
    end
  end  
    
end  
