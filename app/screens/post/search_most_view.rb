
class SearchMostViewScreen < PM::InnerScreen
  include ScreenSharedMethods
  include ImageHelper
  
  def on_load
    rmq.stylesheet = PostStylesheet
    add_search_top_menu(2)
    add_search_table("most_view")
    add_search_common_content 
  end  

  def on_init 
    $search_data = nil
    default_nav_items  
    @current_screen = self
    @current_user = CurrentUser.last
  end

  def textFieldShouldReturn(textField)
    $search_data = textField.text.downcase.strip
    @results_table.reload_all
    textField.text = ""
    textField.resignFirstResponder
    return false
  end  

end  
