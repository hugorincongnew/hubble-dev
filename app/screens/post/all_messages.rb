
class AllMessagesScreen < PM::InnerScreen
  attr_accessor :post
  include ScreenSharedMethods
  include ImageHelper
  include CommonPostMethods
  
  def on_load
    @post = Post.where(:id).eq(self.post).last
    rmq.stylesheet = PostStylesheet
    add_post_data(@post)
    $api.get_post_comments(@post.id, self)
    add_post_message_form
  end  
  
  def update_comments(data)
    @message_container.remove
    add_messages(data)
  end  
  
  def main_video(url)
    BW::Media.play(url, {:scaling_mode => MPMovieScalingModeAspectFill, :control_style => MPMovieControlStyleNone, :movie_source_type => MPMovieSourceTypeStreaming}) do |media_player|
      media_player.view.frame = [[0, 0], [Device.screen.width, Device.screen.height]]
      rmq.append(media_player.view).on(:tap){|sender| media_player.play }
    end
  end  
  
  def add_post_data(data) 
    data.file_type == 0 ? main_image(data.file) : main_video(data.file)
    add_user_info(data.get_user)
    rmq.append(UIView, :top_line)
  end  
  
  def add_messages(all_messages)
    #TEMP DATA
    all_messages = formated_comments(all_messages)

    messages_table = add_child_screen(MessagesTableScreen.new(:nav_bar => false, :messages => all_messages, :post => @post))
    @message_container = rmq.append(messages_table.view, :messages_table_full).layout(:top => (Device.screen.height - 60) - (table_height(all_messages) + 70), :height => table_height(all_messages))
  end  
  
  def add_post_message_form
    rmq.append(UIView, :post_message_bg)    
    b = rmq.append!(UITextField, :post_message)
    b.delegate = self
  end
  
  def textFieldShouldReturn(textField)
    $api.post_new_comment({:comment => {:user_id => @current_user.id, :description => textField.text, :post_id => @post.id}}, self)
    textField.text = ""
    set_view_normal(@current_screen.view, 64)
    textField.resignFirstResponder
    return false
  end  
  
  def textFieldDidBeginEditing(textField)
    top = textField.frame.origin.y.to_i
    animate_screen_for_field_for(top)
  end
  
  def animate_screen_for_field_for(top)
    move = (top - move_for_phone) * -1
    if move < 0
      rmq(@current_screen.view).animate(
        duration: 0.3,
        animations: lambda{|v|
          v.move top: move 
        }
      )
    end  
  end     
  
  def move_for_phone
    case iphone_model    
    when "Iphone4"
      180
    when "Iphone6"
      355 
    when "Iphone6plus"
      410
    else
      255     
    end
  end  
  
  def on_init 
    @current_screen = self
    @current_user = CurrentUser.last 
    default_nav_items    
  end
  
  def formated_comments(all_messages)
    all_messages.map{|m| {:id => m["id"], :message => m["description"], :user_id => m["user"]["id"], :username => m["user"]["username"], :avatar => m["user"]["avatar"]}}
  end 

end  
