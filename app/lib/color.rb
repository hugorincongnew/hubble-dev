module Color
  module_function
  
  def background
    black
  end
  
  def black
    "#000000".to_color
  end  
  
  def primary_color
    "#f0f3f4".to_color
  end  
  
  def main_grey
    "#c2c3c8".to_color
  end  
  
  def btn_red
    "#df282b".to_color
  end 
  
  def home_form_lines
    white
  end  
  
  def nav_bar_bg
    white 
  end
  
  def nav_bar_icons
    "#116788".to_color
  end  
  
  def white 
    "#ffffff".to_color
  end  
 
  def border_color
    "#454744".to_color
  end  
  
  def border_color2
    "#BEBEBF".to_color
  end  
  
  def follow_color
    "#ACADAF".to_color
  end  
  
  def username_blue
    "#374A5C".to_color
  end  
  
  def orange_title
    "#ee614f".to_color
  end  
  
  def bottom_nav_bg_color
    "#F0634F".to_color
  end  
  
  def bottom_nav_active
    "#F2765C".to_color
  end    
  
  def inner_top_bar
    "#0c151f".to_color
  end  
  
  def profile_bg
    "#F1F2F4".to_color
  end  
  
  def clear_orange
    "#F1D8D5".to_color
  end  
  
  def follow_btn_color
    "#929292".to_color
  end  
  
  def wizard_orange
    "#FF6F59".to_color
  end  
  
  def gray3
    "#828385".to_color
  end  
  
end
