class API
  include FormValidation
  include CDQ
  include PM::ScreenModule
  
  def initialize
    NSLog("Initializing the api Wraper")
    @host = "https://hubble-staging.herokuapp.com"
    @base_screen = nil
  end
  
  def sign_out(screen)
    AFMotion::JSON.delete("#{@host}/users/sign_out") do |result|  
      screen.close_session
    end  
  end  
  
  def get_location_in(lat, long)
    AFMotion::JSON.get("http://maps.googleapis.com/maps/api/geocode/json?latlng=#{lat},#{long}&sensor=true") do |result|
      $current_location = location_from_json(result.object)
    end
  end  
  
  def location_from_json(json)
     "#{json[:results][0][:address_components][2][:long_name]} #{json[:results][0][:address_components][4][:short_name]}"
  end  
  
  def sign_in(data, screen)
    @base_screen = screen
    $notifier.show("Loging...", :black)
    abstract_post_api_caller("users/sign_in", data, "Authenticated") if valid_signin?(data)
  end
  
  def register(data, screen)
    @base_screen = screen
    $notifier.show("Creating Account...", :black)
    response = abstract_post_api_caller("api/users", data) if valid_register?(data)
  end  
  
  def forgot_password(data, screen)
    @base_screen = screen
    $notifier.show("Sending Info...", :black)
    response = abstract_post_api_caller("users/password", data, nil, :code_screen) if valid_forgot_password?(data)
  end
  
  def code_password(data, screen)
    @base_screen = screen
    $notifier.show("Sending Code...", :black)
    response = abstract_post_api_caller("users/password/verify_code", data) if valid_code?(data)
  end  
  
  def reset_password(data, screen)
    @base_screen = screen
    $notifier.show("Sending New Password...", :black)
    response = abstract_put_api_caller("users/password", data, nil, :login_screen) if valid_reset_password?(data)
  end  
  
  def get_trending(screen, page=1)   
    main_client.get("api/search/trending?page=#{page}&search=#{$search_data}"){|result| screen.update_info(result.object)}
  end 
  
  def get_most_view(screen, page=1)
    main_client.get("api/search/most_viewed?page=#{page}&search=#{$search_data}") do |result| 
      screen.update_info(result.object)
    end  
  end  
  
  def get_most_like(screen, page=1)
    main_client.get("api/search/most_liked?page=#{page}&search=#{$search_data}"){|result| screen.update_info(result.object)}
  end       
  
  def get_dashboard_info(data, screen, page=1)  
    main_client.get("api/dashboard/#{CurrentUser.get.id}?page=#{page}") do |result| 
      screen.update_info(result.object)
    end  
  end  
  
  def set_new_view(data)
    main_client.post("#{@host}/api/views", data){|result| puts result.object.inspect}
  end  
  
  def set_new_like(data, screen)
    main_client.post("#{@host}/api/likes", data) do |result| 
      response = result.object
      post = Post.where(:id).eq(response[:like][:post_id].to_i).last
      post.like = response[:info] == "Like Created" ? "true" : "false"
      cdq.save
      screen.get_new_info
    end
  end    
  
  def new_post(data, screen)
    @base_screen = screen
    $notifier.show("Creating Post.", :black)
    response = client_abstract_post_api_caller("api/posts", data, nil, :go_dashboard)
  end  
  
  def main_client
    client = AFMotion::SessionClient.build("#{@host}/") do
      header "X-User-Email", CurrentUser.get.email
      header "X-User-Token", CurrentUser.get.auth_key     
    end  
    return client
  end  
  
  def update_follows
    main_client.get("api/users/#{CurrentUser.get.id}/following"){|result| CurrentUser.update_follow(result.object)}
  end     
  
  def update_user_info(user_id, screen)
    main_client.get("api/users/#{user_id}"){|result| User.update_info(result.object, screen)}
  end  
  
  def get_user_feed(data, screen)
    main_client.get("api/feeds/#{data[:user_id]}"){|result| screen.update_info(result.object)}
  end
  
  def follow(user, screen)
    follow_action(user, screen)
  end  
  
  def unfollow(user, screen)
    follow_action(user, screen)
  end    
  
  def follow_action(user, screen)
    data = {:user_id => CurrentUser.get.id, :followed_id => user.id}
    main_client.post("/api/active_relationships", data) do |result|
      screen.follow_unfollow_response(result.object)
    end 
  end  

  def following(user_id, screen)
    main_client.get("api/users/#{user_id}/following") do |result|   
      puts result.object.inspect 
    end   
  end  
  
  def followers(user_id, screen)
    main_client.get("api/users/#{user_id}/followers") do |result| 
      puts result.object.inspect 
    end   
  end   
  
  def get_post_comments(post_id, screen)
    main_client.get("api/posts/#{post_id}/comments") do |result| 
      screen.add_messages(result.object)
    end 
  end  
  
  def post_new_comment(data, screen)
    main_client.post("api/comments", data) do |result| 
      screen.update_comments(result.object)
    end 
  end  
  
  def update_current_user
     main_client.get("api/users/#{CurrentUser.last.id}"){|result| CurrentUser.update_current_user(result.object)}
  end   

  private
  
  def action_for(endpoint)
    case endpoint
      when "users/sign_in"
        :login_user
      when "api/users"
        :register_user   
      when "users/password/verify_code"  
        :set_new_password
    end
  end  
  
  def response_action(data, action)
    send(action, data)
  end  
  
  def login_user(data)
    CurrentUser.set_or_create_currentuser(data["user"])
    @base_screen.open_dashboard 
  end
  
  def register_user(data)
    CurrentUser.set_or_create_currentuser(data["user"])
    @base_screen.open_dashboard 
  end    
  
  def set_new_password(data)
    @base_screen.open_new_password   
  end  
  
  def login_screen(data)
    @base_screen.go_to_login 
  end  
  
  def code_screen(data)
    @base_screen.go_to_code
  end   
  
  def go_dashboard(data)
    @base_screen.close to_screen: :root
  end  
  
  def abstract_get_api_caller(endpoint)
    AFMotion::JSON.get("#{@host}/#{endpoint}") do |result|
      NSLog "RESPONSE:"
      NSLog result.object.inspect
    end
  end
  
  def abstract_post_api_caller(endpoint, data, success_mesage=nil, action=nil)
    NSLog "SENDING POST REQUEST"
    AFMotion::JSON.post("#{@host}/#{endpoint}", data) do |result|
      handle_results(result, success_mesage, action, endpoint)
    end
  end
  
  def client_abstract_post_api_caller(endpoint, data, success_mesage=nil, action=nil)
    NSLog "SENDING POST REQUEST"
    main_client.post("#{@host}/#{endpoint}", data) do |result|
      handle_results(result, success_mesage, action, endpoint)
    end
  end  
  
  def abstract_put_api_caller(endpoint, data, success_mesage=nil, action=nil)
    NSLog "SENDING PUT REQUEST"
    AFMotion::JSON.put("#{@host}/#{endpoint}", data) do |result|
      handle_results(result, success_mesage, action, endpoint)
    end
  end  
  
  def handle_results(result, success_mesage, action, endpoint)
    error_message="response error"    
    NSLog "RESPONSE:"
    NSLog result.object.inspect
    
    unless result.object.nil?
      if result.object["success"] == true
        $notifier.success(success_mesage)
        response_action(result.object, !action.nil? ? action : action_for(endpoint))
      else
        error_message = result.object["info"] unless result.object["info"].nil?
        $notifier.error(error_message)
      end    
    else    
      $notifier.error("There was a problem communicating with the server, please try againg.")
    end 
  end     
  
end
