module ImageHelper
  
  def loading_comments_image(container)
    placeholder = loading_animation
    load_for_container(CGRectMake(0, 0, 0, 0), nil, placeholder, container)
  end  
  
  def load_activity_image(url)
    frame = CGRectMake(Device.screen.width - 90, 0, 35, 62)
    placeholder = placeholder_for_image(UIImage.imageNamed("bg.png"), frame)
    load_for_container(frame, url, placeholder, self)
  end    
  
  def load_dashboard_profile_image(url)
    rmq.append(UIView, :profile_container_border)
    profile_container = rmq.append(UIView, :profile_container).get
    frame = CGRectMake(0, 0, 40, 40)
    placeholder = placeholder_for_image(UIImage.imageNamed("icon-profile.png"), frame)
    load_for_container(frame, url, placeholder, profile_container)
  end

  def load_profile_image(url, container)
    container.append(UIView, :profile_container_img_border)
    profile_container = container.append(UIView, :profile_container_img).get
    frame = CGRectMake(0, 0, 50, 50)
    placeholder = placeholder_for_image(UIImage.imageNamed("icon-profile.png"), frame)
    load_for_container(frame, url, placeholder, profile_container)
  end
  
  def mini_profile_image(url, styleclass, container)
    profile_container = rmq.append(UIView, styleclass).get
    frame = CGRectMake(0, 0, 30, 30)
    placeholder = placeholder_for_image(UIImage.imageNamed("icon-profile.png"), frame)
    load_for_container(frame, url, placeholder, profile_container)
  end  
  
  def micro_profile_image(url, styleclass, container)
    profile_container = rmq.append(UIView, styleclass).get
    frame = CGRectMake(0, 0, 20, 20)
    placeholder = placeholder_for_image(UIImage.imageNamed("icon-profile.png"), frame)
    load_for_container(frame, url, placeholder, profile_container)
  end    
  
  def load_dashboard_image(url)
    placeholder = loading_animation
    load_for_container(CGRectMake(0, 0, Device.screen.width, ((Device.screen.height - 60) - 2)), url, placeholder)
  end  
  
  def load_fullview_image(url)
    placeholder = loading_animation
    load_for_container(CGRectMake(0, -20, Device.screen.width, Device.screen.height), url, placeholder)
  end    
  
  def loading_animation
    loading = UIImageView.alloc.initWithFrame(CGRectMake(media_screen - 25, 175, 50, 50)) 
    loading.animationImages = loading_images_array
                               
    loading.animationDuration = 1.5
    loading.animationRepeatCount = 0
    loading.startAnimating
    loading
  end  
  
  private
  
  def load_for_container(frame, url, placeholder, container=nil, rounded=false)
    if url
      AFMotion::Image.get(url) do |result|
        rmq(placeholder).remove
        image_view = UIImageView.alloc.initWithFrame(frame)   
        image_view.contentMode = UIViewContentModeScaleAspectFill
        image_view.image = result.object
        prepend_item(image_view, container)
      end
    end  
    prepend_item(placeholder, container)    
  end  
  
  def prepend_item(item, container=nil)
    container.nil? ? rmq.prepend(item) : container.rmq.prepend(item) 
  end  
  
  def loading_images_array
    (1..11).map{|n| UIImage.imageNamed("loading#{n}.gif")}
  end  
  
  def placeholder_for_image(image, frame)
    placeholder = UIImageView.alloc.initWithFrame(frame)
    placeholder.image = image
    return placeholder 
  end  
  
  def media_screen
    Device.screen.width / 2
  end  
  
end
