module FormValidation
  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  
  def valid_signin?(data)
    return alert_message(invalid_message(is_a_valid_email?(data[:user][:email].to_s))) if is_a_valid_email?(data[:user][:email].to_s)
    return alert_message(invalid_message(is_a_valid_password?(data[:user][:password].to_s))) if is_a_valid_password?(data[:user][:password].to_s)
    return true
  end  
  
  def valid_register?(data)
    return alert_message(invalid_message(is_a_valid_email?(data[:user][:email].to_s))) if is_a_valid_email?(data[:user][:email].to_s)
    return alert_message(invalid_message(is_a_valid_register_password?(data[:user][:password].to_s, data[:user][:password_confirmation].to_s))) if is_a_valid_register_password?(data[:user][:password].to_s, data[:user][:password_confirmation].to_s) 
    return alert_message(invalid_message(is_a_valid_username?(data[:user][:username].to_s))) if is_a_valid_username?(data[:user][:username].to_s)
    return alert_message(invalid_message(is_a_valid_full_name?(data[:user][:full_name].to_s))) if is_a_valid_full_name?(data[:user][:full_name].to_s)
    return true
  end    
  
  def valid_forgot_password?(data)
    return alert_message(invalid_message(is_a_valid_email?(data[:user][:email].to_s))) if is_a_valid_email?(data[:user][:email].to_s)
    return true
  end 
  
  def valid_reset_password?(data)
    return alert_message(invalid_message(is_a_valid_register_password?(data[:user][:password].to_s, data[:user][:password_confirmation].to_s))) if is_a_valid_register_password?(data[:user][:password].to_s, data[:user][:password_confirmation].to_s) 
    return true
  end   
  
  def valid_code?(data)
    return alert_message(invalid_message(is_a_valid_code?(data[:user][:reset_password_code].to_s))) if is_a_valid_code?(data[:user][:reset_password_code].to_s)
    return true
  end     

  def is_a_valid_password?(password)
    return :empty_password if password.length == 0
    return :short_password if password.length < 8
  end  
  
  def is_a_valid_code?(code)
    return :empty_code if code.length == 0
  end  
  
  def is_a_valid_register_password?(password, password_confirmation)  
    return :empty_password if password.length == 0 or password_confirmation.length == 0
    return :short_password if password.length < 8 or password_confirmation.length < 8  
    return :same_password if password != password_confirmation
  end  
  
  def is_a_valid_email?(email)
    return :empty_email if email.length == 0
    return :invalid_email unless !(email =~ VALID_EMAIL_REGEX).nil?
  end
  
  def is_a_valid_full_name?(full_name)
    return :empty_name if full_name.length == 0
  end  
  
  def is_a_valid_username?(username)
    return :empty_username if username.length == 0
    return :short_username if username.length < 3
  end      
  
  def invalid_message(name)
    messages = {:invalid_email  => "Please enter a valid email.",
                :empty_email    => empty_message_for("Email"),
                :empty_code     => empty_message_for("Code"),            
                :empty_password => empty_message_for("Password"),
                :empty_username => empty_message_for("Username"),
                :empty_name     => empty_message_for("Full Name"),            
                :short_password => "Please choose a password with at least 8 characters",
                :short_username => "Please choose a username with at least 3 characters",
                :same_password  => "Password confirmation doesn't match Password"}
    return messages[name] if messages[name]
  end
  
  def empty_message_for(field)
    "#{field.capitalize} field can't be empty"
  end  
  
  def alert_message(message)
    $notifier.error(message)
    false
  end    
end  