class Trending < CDQManagedObject
  
  def self.set_new(data)
    self.clean
    data.map{|p| add_new(p)}
    return Trending.all
  end  
  
  def self.clean
    Trending.destroy_all
  end    
  
  def self.add_new(post)
    User.find_or_create(post[:user])
    Comment.find_or_create_new(post[:comment])
    Trending.create(:id => post[:id], :user_id => post[:user_id], :likes => post[:likes_count].to_i, :views => post[:views_count].to_i, :title => post[:description], :location => post[:location], :file => post[:file][:url], :file_type => post[:post_type_id])
    cdq.save
  end    
  
  def get_user
    User.where(:id).eq(self.user_id.to_i).last
  end    
  
end