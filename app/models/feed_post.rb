class FeedPost < CDQManagedObject

  def self.set_new(data)
    self.clean
    data.map{|p| add_new(p)}
    return FeedPost.all
  end  
  
  def self.find_or_create(post_data)
    post = FeedPost.where(:id).eq(post_data[:id].to_i).last
    return post.nil? ? add_new(post_data) : post
  end  
  
  def self.clean
    Post.destroy_all
  end    
  
  def self.add_new(post)
    User.find_or_create(post[:user])
    Comment.find_or_create_new(post[:comment])
    FeedPost.create(:id => post[:id], :user_id => post[:user_id], :likes => post[:likes_count].to_i, :views => post[:views_count].to_i, :title => post[:description], :location => post[:location], :file => post[:file][:url], :file_type => post[:post_type_id])
    cdq.save
  end   
  
  def get_user
    User.where(:id).eq(self.user_id.to_i).last
  end 
  
end