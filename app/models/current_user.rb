class CurrentUser < CDQManagedObject

  def self.set_or_create_currentuser(user)
    
    if CurrentUser.last.nil?
      CurrentUser.create(:id => user["id"], :email => user["email"], :username => user["username"], :full_name => user["full_name"], :auth_key => user["authentication_token"])
      cdq.save
    else
      current_user = CurrentUser.first
      current_user.id = user["id"]
      current_user.email = user["email"]
      current_user.username = user["username"]
      current_user.full_name = user["full_name"]
      current_user.auth_key = user["authentication_token"]
      current_user.avatar = user["avatar"]["avatar"]["url"]
      current_user.followers = user["followers"]   
      current_user.following = user["following"]   
      cdq.save
    end  
  end   
  
  def self.update_current_user(data)
    
    user = data["user"]
    current_user = CurrentUser.first
    current_user.id = user["id"]
    current_user.email = user["email"]
    current_user.username = user["username"]
    current_user.full_name = user["full_name"]
    current_user.auth_key = user["authentication_token"]
    current_user.avatar = user["avatar"]["avatar"]["url"]
    current_user.followers = data["followers"]   
    current_user.following = data["following"]   
    cdq.save
  end  
  
  def self.update_follow(data) 
    data["following"].each{|u| self.set_follow(u)}
  end  
  
  def self.set_follow(user)
    User.add_or_update_follow(user)    
  end  
  
  def self.get
    CurrentUser.last
  end  
  
  def profile_name
    self.full_name.split(" ").map{|w| w.capitalize}.join(" ")
  end 
  
end