class User < CDQManagedObject
  
  def self.find_or_create(user_data)
    if user_data
      user = User.where(:id).eq(user_data[:id].to_i).last
      return user.nil? ? create_new_user(user_data) : user
    end  
  end
  
  def self.create_new_user(user)  
    User.create(:id => user[:id], :email => user[:email], :username => user[:username], :full_name => user[:full_name], :avatar => user[:avatar][:url])
    cdq.save
  end  
  
  def self.add_or_update_follow(user_data)
    user =  User.where(:id).eq(user_data["id"]).last
    if user.nil?   
      self.create_new_user(user_data) 
      cdq.save
      user =  User.where(:id).eq(user_data["id"]).last
    end    
    cdq.save
    user.follow = "true"
  end
  
  def self.unfollow(user_id)
    user =  User.where(:id).eq(user_id).last
    user.follow = "false"
    cdq.save
  end  
  
  def profile_name
    self.full_name.split(" ").map{|w| w.capitalize}.join(" ")
  end    
  
  def following_user?
    User.where(:id).eq(self.id.to_i).last.follow == "true"
  end  
  
  
  def self.update_info(data, screen)
    user =  User.where(:id).eq(data["user"]["id"]).last
    user.username = data["user"]["username"]
    user.full_name = data["user"]["full_name"]
    user.avatar = data["user"]["avatar"]["url"]
    user.followers = data["followers"]
    user.following = data["following"]
    cdq.save
    screen.refresh_info
  end  
end