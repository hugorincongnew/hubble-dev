class Post < CDQManagedObject

  def self.set_new(data)
    self.clean
    data.map{|p| add_new(p)}
    return Post.all
  end  
  
  def self.find_or_create(post_data)
    post = Post.where(:id).eq(post_data.id).last
    return post.nil? ? add_new_from_obj(post_data) : post
  end  
  
  def self.clean
    Post.destroy_all
  end
      
  def self.add_new_from_obj(post)
    Comment.find_or_create_new(post.comments)
    Post.create(:id => post.id, :user_id => post.user_id, :likes => post.likes, :views => post.views, :title => post.title, :location => post.location, :file => post.file, :file_type => post.file_type)
    cdq.save
    Post.last
  end
  
  def self.add_new(post)
    User.find_or_create(post[:user])
    Comment.find_or_create_new(post[:comment])
    Post.create(:id => post[:id], :user_id => post[:user_id], :likes => post[:likes_count].to_i, :views => post[:views_count].to_i, :title => post[:description], :location => post[:location], :file => post[:file][:url], :file_type => post[:post_type_id])
    cdq.save
    Post.last
  end   
  
  def get_user
    User.where(:id).eq(self.user_id.to_i).last
  end  
  
  def self.my_posts
    Post.where(:user_id).eq(CurrentUser.get.id)
  end  

end