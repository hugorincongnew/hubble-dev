class Feed < CDQManagedObject
  
  def self.set_new(data)
    self.clean
    data.to_a.map{|p| add_new(p)}
    return Feed.all
  end  
  
  def self.add_new(feed) 
    User.find_or_create(feed[:user])
    FeedPost.find_or_create(feed[:post])
    Feed.create(:id => feed[:id], :post_id => feed[:post_id], :feed_type => feed[:feed_type], :user_id => feed[:user][:id])
    cdq.save
  end    
  
  def self.clean
    Feed.destroy_all
  end    
  
  def user
    User.where(:id).eq(self.user_id).last
  end  
  
  def post
    FeedPost.where(:id).eq(self.post_id).last
  end  
   
end