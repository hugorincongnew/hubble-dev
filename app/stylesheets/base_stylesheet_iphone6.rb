class BaseStylesheetIphone6 < BaseStylesheet

  def inputs(st)
    st.width = Device.screen.width * 0.8 - 30
    st.height = 40
    st.text_color = Color.main_grey
    st.left = input_left(st.width)
    st.font = rmq.font.font_with_name('Roboto-Regular', 16)
  end
  
  def logo(st)
    st.width = logo_size_w
    st.height = logo_size_h
    st.left = logo_left   
    st.top = 100
    st.image = image.resource("logo")
  end
  
  def new_account_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 16)
  end
  
  def create_account_line(st)
    st.width = 162
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end    
  
  def back_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end    
  
  def back_to_login_line(st)
    st.width = 180
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end     
  
  def forgot_password_title(st)
    st.left = 0
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 18)
    st.text = "FORGOT YOUR PASSWORD?"
    st.text_alignment = NSTextAlignmentCenter
  end    
  
  def forgot_btn(st)
    st.width = 90
    st.height = 40
    st.color = Color.main_grey
    st.left = 260
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end    
  
end