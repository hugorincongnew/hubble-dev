class DashboardStylesheet < InnerBaseStylesheet

  def profile_container(st)
    st.width = 40
    st.height = 40
    st.top = 30
    st.left = 17
    st.masks_to_bounds = true
    st.corner_radius = 20
  end  

  def profile_container_border(st)
    profile_container st
    st.width = 44
    st.height = 44
    st.top = 28
    st.left = 15
    st.masks_to_bounds = true
    st.corner_radius = 22
    st.background_color = Color.border_color
  end  
  
  def line(st)
    st.width = Device.screen.width
    st.height = 2
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end
  
  def bottom_line(st)
    line st
    st.width = Device.screen.width + 50
    st.top = ((Device.screen.height - 60) - 2)
  end    
  
  def top_line(st)
    line st
    st.top = 0
  end
  
  def username(st)
    st.top = 30
    st.left = 65
    st.width = (Device.screen.width * 0.7)
    st.height = 40    
    st.color = Color.orange_title
    st.font = rmq.font.font_with_name('Roboto-Black', 17)
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft
  end  
    
  def dashboard_icon(st)
    st.left = 24
    st.width = 26
    st.height = 26
  end  
  
  def location_icon(st)
    dashboard_icon st
    st.top = 90
    st.image = image.resource("icon_location")   
  end  
  
  def views_icon(st)
    dashboard_icon st
    st.top = 130
    st.image = image.resource("icon_views")   
  end    
  
  def likes_icon(st)
    dashboard_icon st
    st.top = 170
    st.image = image.resource("icon_likes")   
  end    
  
  def likes_icon_red(st)
    dashboard_icon st
    st.top = 170
    st.image = image.resource("icon_likes_red")   
  end   
  
  def comments_icon(st)
    dashboard_icon st
    st.top  = 210
    st.image = image.resource("icon_comments")   
  end  
  
  def labels(st)
    st.height = 26
    st.left = 68
    st.width = 200
    st.text_alignment = NSTextAlignmentLeft
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.color = Color.white    
    st.view.shadowColor = Color.black
    st.view.shadowOffset = CGSizeMake(1, 1)
    st.view.layer.shadowRadius = 5.0
    st.view.layer.shadowOpacity = 1.0
  end  
  
  def location(st)
    labels st
    st.top = 90
  end  
  
  def views(st)
    labels st
    st.top = 130
  end 
  
  def likes(st)
    labels st
    st.top = 170
  end 
  
  def comments(st)
    labels st
    st.top = 210
  end      
  
  def activity_profile_image(st)
    st.width = 20
    st.height = 20
    st.top = 10
    st.left = 10
    st.masks_to_bounds = true
    st.corner_radius = 10
  end  
  
  def activity_username(st)
    st.top = 10
    st.left = 35
    st.width = (Device.screen.width * 0.7)
    st.height = 20    
    st.color = Color.username_blue
    st.font = rmq.font.font_with_name('Roboto-Black', 14)
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft
  end     
  
  def like_comment_label(st)
    st.left = 35
    st.top = 30
    st.width = (Device.screen.width * 0.7)
    st.height = 20  
    st.color = Color.gray3
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end 
  
  def picture_preview(st)
    
    st.width = 30
    st.height = 30
    st.top = 10
    st.left = 200
  end   
  
  def profile_nav_container(st)
    st.width = 120
    st.height = 90
    st.left = Device.screen.width - 120
    st.top = -90
    st.background_color = Color.black
  end  

  def profile_nav_container_bg(st)
    st.width = 122
    st.height = 92
    st.left = Device.screen.width - 121
    st.top = -92
    st.background_color = Color.gray3
  end   
  
  def messages_nav_btn(st)
    setting_btn st
    st.top = 0
  end
  
  def settings_nav_btn(st)
    setting_btn st
    st.top = 30
  end
  
  def logout_nav_btn(st)
    setting_btn st
    st.top = 60
  end   
  
  def setting_btn(st)
    st.width = 120
    st.height = 30
    st.left = 0
    st.color = Color.white    
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)    
  end     
  
  def profile_nav_line1(st)
    profile_nav_line st
    st.top = 30
  end
  
  def profile_nav_line2(st)
    profile_nav_line st
    st.top = 60    
  end
  
  def profile_nav_line(st)
    st.background_color = Color.gray3
    st.height = 1
    st.width = 80
    st.left =  20
  end      
end