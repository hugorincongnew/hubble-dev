class StreamingStylesheet < DashboardStylesheet
  
  def record_button(st)
    st.width = 20
    st.height = 20
    st.left = 50
    st.top = 50
    st.background_color = Color.white
  end  
end