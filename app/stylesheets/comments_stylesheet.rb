class CommentsStylesheet < RubyMotionQuery::Stylesheet
  
  def main_bg(st)
    st.left= 0
    st.top = 0
    st.height = 55
    st.width = (Device.screen.width / 2) + 40
  end  
  
  def message_container(st)
    message_container_style st
    st.height = 50
    st.left = 5
    st.top = 5
  end  
  
  def message_container_style(st)
    st.background_color = Color.black
    st.opacity = 0.8
    st.width = (Device.screen.width / 2) + 30
    st.left =  0
  end   
  
  def messages_profile_image(st)
    st.width = 30
    st.height = 30
    st.top = 15
    st.left = (Device.screen.width / 2) - 5
    st.masks_to_bounds = true
    st.corner_radius = 15
  end  

  def comment(st)
    message_style st    
    st.top = 22
    st.color = Color.white    
  end
  
  def username(st)
    message_style st
    st.top = 5
    st.color = Color.white    
  end  
  
  def owner_username(st)
    message_style st
    st.top = 5
    st.color = Color.orange_title   
  end    
  
  def message_style(st)
    st.height = 20
    st.left = 0
    st.width = (Device.screen.width / 2) - 25
    st.text_alignment = NSTextAlignmentRight   
    st.font = rmq.font.font_with_name('Roboto-Regular', 12) 
  end  
  
end