class PostStylesheet < DashboardStylesheet
  
  def post_title_bg(st)
    st.width = Device.screen.width
    st.height = 60
    st.left = 0
    st.top  = Device.screen.height - 120
    st.background_color = Color.black
    st.opacity = 0.8
  end
  
  def post_title(st)
    st.width = Device.screen.width
    st.height = 60
    st.top = 0
    st.left = 20
    st.color = Color.white
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)    
    st.text_alignment = :left
  end    

  def messages_table(st)
    messages_table_style st
    st.view.scrollEnabled = false
  end   
  
  def messages_table_full(st)
    messages_table_style st
    st.view.scrollEnabled = true
  end     
  
  def messages_table_style(st)
    st.width = (Device.screen.width / 2) + 40
    st.left =  Device.screen.width / 2 - 45
    st.view.setBackgroundView(nil)
    st.view.setBackgroundColor(UIColor.clearColor)
    st.separator_style = UITableViewCellSeparatorStyleNone
  end   

  def all_messages(st)
    st.height = 35
    st.color = Color.orange_title
    st.font = rmq.font.font_with_name('Roboto-Bold', 14)    
    st.background_color = Color.black
    st.opacity = 0.8
    st.width = (Device.screen.width / 2) + 30    
    st.left = Device.screen.width / 2 - 40
  end
  
  def post_message_bg(st)  
    st.width = Device.screen.width
    st.height = 55
    st.left = 0    
    st.top = Device.screen.height  - 120
    st.background_color = Color.white    
  end
  
  def post_message(st)
    st.width = Device.screen.width - 10
    st.height = 55
    st.left = 10
    st.top = Device.screen.height - 120
    st.text_color = Color.black
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.background_color = Color.white
    st.placeholder = "Type your message..."
  end  
  
  def comment_section(st)
    st.width = Device.screen.width / 2
    st.height = Device.screen.width / 2
    st.top = Device.screen.height - 420
    st.left = (Device.screen.width / 2) - 120
  end  
  
  def streaming_container(st)
    st.top = 0
    st.height = Device.screen.height - 130
    st.width = Device.screen.width
    st.left = 0
    st.background_color = Color.white
  end  
end