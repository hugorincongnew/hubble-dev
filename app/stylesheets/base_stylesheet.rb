class BaseStylesheet < RubyMotionQuery::Stylesheet

  def inputs(st)
    st.width = Device.screen.width * 0.8 - 30
    st.height = 40
    st.text_color = Color.main_grey
    st.left = input_left(st.width)
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end
  
  def logo(st)
    st.width = logo_size_w
    st.height = logo_size_h
    st.left = logo_left   
    st.top = 50
    st.image = image.resource("logo")
  end
  
  def new_account_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end  
  
  def create_account_line(st)
    st.width = 142
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end   
  
  def red_btn(st)
    st.width = Device.screen.width
    st.height = 62
    st.left = 0
    st.top = Device.screen.height - st.height
    st.background_color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Bold', 22)
    st.color = Color.btn_red
  end  
  
  def form_icon(st)
    st.width = 20
    st.height = 20
    st.left = (input_left(Device.screen.width)) + 20
  end    

  def user_icon(st)
    form_icon st
    st.image = image.resource("icon_user")
  end
  
  def email_icon(st)
    form_icon st
    st.image = image.resource("icon_mail")
  end
  
  def password_icon(st)
    form_icon st
    st.image = image.resource("icon_password")    
  end    
  
  def back_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end    
  
  def back_to_login_line(st)
    st.width = 156
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end     
    
  def forgot_password_title(st)
    st.centered = :horizontal
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end    
  
  def reset_password_title(st)
    forgot_password_title st
    st.text = "RESET YOUR PASSWORD"
  end    
  
  def code_password_title(st)
    forgot_password_title st
    st.text = "CHECK YOUR EMAIL"
  end     
  
  def code_password_subtitle(st)
    st.left = 0
    st.width = Device.screen.width
    st.height = 40
    st.text_alignment = NSTextAlignmentCenter    
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)    
    st.text = "WE SENT A VERIFICATION CODE"
  end    
  
  def forgot_btn(st)
    st.width = 90
    st.height = 40
    st.color = Color.main_grey
    st.left = 215
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end    
  
  private
  
  def input_left(width)
    (Device.screen.width / 2) - (width / 2) + 20
  end     
  
  def logo_size_w
    (Device.screen.width * 0.50)
  end  
  
  def logo_size_h
    (logo_size_w * 107) / 335
  end  
  
  def logo_left
    (Device.screen.width / 2) - (logo_size_w / 2)
  end    
  
end