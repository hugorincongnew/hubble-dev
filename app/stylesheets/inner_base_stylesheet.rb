class InnerBaseStylesheet < RubyMotionQuery::Stylesheet
  
  def menu_container(st)
    st.height = 60
    st.width = Device.screen.width
    st.left = 0
    st.top = Device.screen.height
  end  
  
  
  def menu_container_active(st)
    st.height = 70
    st.width = Device.screen.width
    st.left = 0
    st.top = Device.screen.height - 130
  end  
  
  def menu_bg(st)
    st.height = 60
    st.width = Device.screen.width
    st.left = 0
    st.top = 0
    st.background_color = Color.black
    st.opacity = 0.3
  end  
  
  def inner_menu_container(st)
    st.height = 60
    st.width = Device.screen.width
    st.left = 0
    st.top = Device.screen.height - 125
  end  
  
  def inner_menu_bg(st)
    st.height = 60
    st.width = Device.screen.width
    st.left = 0
    st.top = 0
    st.background_color = Color.bottom_nav_bg_color
  end  
  
  def inner_top_bar(st)
    st.width = Device.screen.width
    st.height = 45
    st.left = 0
    st.top = 0
    st.background_color = Color.inner_top_bar
  end  
  
  def select_from_library(st)
    right_bar_button st
    st.text = "FROM GALLERY"
  end  
  
  def select_from_photo(st)
    right_bar_button st
    st.text = "TAKE PHOTO"
  end  
  
  def select_from_video(st)
    right_bar_button st
    st.text = "TAKE VIDEO"
  end      
  
  def publish_file(st)
    right_bar_button st
    st.text = "PUBLISH"
  end    
  
  def cancel_file(st)
    left_bar_button st
    st.text = "X"
    st.font = rmq.font.font_with_name('Roboto-Regular', 15)
  end  
  
  def right_bar_button(st)
    st.top = 0
    st.left = (Device.screen.width / 2) - 15
    st.width = (Device.screen.width / 2)
    st.height = 45 
    st.color = Color.white
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight
  end  
  
  def left_bar_button(st)
    st.top = 0
    st.left = 15
    st.width = (Device.screen.width / 2)
    st.height = 45 
    st.color = Color.white
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft
  end    
  
  def select_from_library_image(st)
    st.width = 100
    st.height = 100
    st.left = (Device.screen.width / 2) - 50
    st.top = (Device.screen.height / 2) - 115
    st.image = image.resource("folder_library")
    st.view.setAlpha(0.5)
  end  
  
  def take_photo_image(st)
    st.width = 100
    st.height = 100
    st.left = (Device.screen.width / 2) - 50
    st.top = (Device.screen.height / 2) - 115
    st.image = image.resource("camera")
    st.view.setAlpha(0.6)
  end  
  
  def take_video_image(st)
    st.width = 100
    st.height = 100
    st.left = (Device.screen.width / 2) - 50
    st.top = (Device.screen.height / 2) - 115
    st.image = image.resource("video")
    st.view.setAlpha(0.6)
  end      
  
  def main_image_container(st)
    st.width = Device.screen.width
    st.top = 45
    st.left = 0
    st.height = Device.screen.height - 270
    st.view.contentMode = UIViewContentModeScaleAspectFill
  end  
  
  def white_line(st)
    st.width = Device.screen.width
    st.height = 2
    st.left = 0
    st.top = 0
    st.background_color = Color.black
  end  
  
  def menu_bg_orange(st)
    st.height = 70
    st.width = Device.screen.width
    st.left = 0
    st.top = 0
    st.background_color = Color.bottom_nav_bg_color
  end    
  
  def search_inner_menu_active_pos1(st)
    search_inner_menu_active st
    st.left = active_position(1)
  end  
  
  def search_inner_menu_active_pos2(st)
    search_inner_menu_active st
    st.left = active_position(2)
  end  
  
  def search_inner_menu_active_pos3(st)
    search_inner_menu_active st
    st.left = active_position(3)
  end  
  
  def search_inner_menu_active_pos4(st)
    search_inner_menu_active st
    st.left = active_position(4)
  end        
  
  def search_inner_menu_active(st)
    st.height = 69
    st.top = 0
    st.width = Device.screen.width / 4
    st.background_color = Color.bottom_nav_active 
  end  
  
  def menu_active_pos1(st)
    menu_active st
    st.left = active_position(1)
  end  
  
  def menu_active_pos2(st)
    menu_active st
    st.left = active_position(2)
  end    
  
  def menu_active_pos3(st)
    menu_active st
    st.left = active_position(3)
  end    
  
  def menu_active_pos4(st)
    menu_active st
    st.left = active_position(4)
  end  
  
  def menu_active(st)
    st.height = 70
    st.top = 0
    st.width = Device.screen.width / 4
    st.background_color = Color.bottom_nav_active   
  end  
  
  def home_btn(st)
    menu_icon_size st
    st.left = menu_btn_position(1)
    st.top = 15
    st.image = image.resource("menu_icon_dashboard") 
  end  
  
  def search_btn(st)
    menu_icon_size st    
    st.left = menu_btn_position(2)
    st.top = 15
    st.image = image.resource("menu_icon_search") 
  end
  
  def photo_btn(st)
    menu_icon_size st    
    st.left = menu_btn_position(3)
    st.top = 15
    st.image = image.resource("menu_icon_camera") 
  end
  
  def profile_btn(st)
    menu_icon_size st    
    st.left = menu_btn_position(4)
    st.top = 15
    st.image = image.resource("menu_icon_profile")  
  end      
  
  def menu_icon_size(st)
    st.width = 34
    st.height = 34 
  end  
  
  def upload_btn(st)
    menu_icon_size st
    st.left = menu_btn_position(1)
    st.top = 15
    st.image = image.resource("menumedia-upload") 
  end    
  
  def camera_btn(st)
    menu_icon_size st
    st.left = menu_btn_position(2)
    st.top = 15
    st.image = image.resource("menumedia-photo") 
  end    
  
  def video_btn(st)
    menu_icon_size st
    st.left = menu_btn_position(3)
    st.top = 15
    st.image = image.resource("menumedia-video") 
  end    
  
  def stream_btn(st)
    menu_icon_size st
    st.left = menu_btn_position(4)
    st.top = 15
    st.image = image.resource("menumedia-stream") 
  end            
  
  def menu_btn_position(n)
    size = (Device.screen.width / 4)
    ((size * n) - 17) - (size / 2)
  end  
  
  def active_position(n)
    size = (Device.screen.width / 4)
    (size * n) - size
  end  
  
  
  
  
  def menu_search_bg_orange(st)
    st.height = 50
    st.width = Device.screen.width
    st.left = 0
    st.top = 0
    st.background_color = Color.bottom_nav_bg_color
  end  
  
  def menu_search_container_active(st)
    st.height = 50
    st.width = Device.screen.width
    st.left = 0
    st.top = 0
  end  
  
  def menu_search_active_pos1(st)
    menu_search_active st
    st.left = search_active_position(1)
  end  
  
  def menu_search_active_pos2(st)
    menu_search_active st
    st.left = search_active_position(2)
  end    
  
  def menu_search_active_pos3(st)
    menu_search_active st
    st.left = search_active_position(3)
  end    
  
  def menu_search_active(st)
    st.height = 50
    st.top = 0
    st.width = Device.screen.width / 3
    st.background_color = Color.bottom_nav_active   
  end    
  
  def search_active_position(n)
    size = (Device.screen.width / 3)
    (size * n) - size
  end  
  
  def trending_btn(st)
    menu_search_btn st
    st.left = menu_serach_btn_position(1)
    st.text = "TRENDING"
  end    

  def most_view_btn(st)
    menu_search_btn st
    st.left = menu_serach_btn_position(2)
    st.text = "MOST VIEWED"
  end    
  
  def most_like_btn(st)
    menu_search_btn st
    st.left = menu_serach_btn_position(3)
    st.text = "MOST LIKED"
  end  
    
  def menu_search_btn(st)
    st.width = Device.screen.width / 3
    st.height = 50
    st.top = 0
    st.font = rmq.font.font_with_name('Roboto-Regular', 13)
    st.color = Color.white
    st.view.titleLabel.setTextAlignment(UITextAlignmentCenter)
  end    
  
  def menu_serach_btn_position(n)
    size = (Device.screen.width / 3)
    (size * n) - size
  end    
  
  def search_input(st)
    st.top = 50
    st.left = 15
    st.width = Device.screen.width
    st.height = 50
    st.background_color = Color.white
    st.placeholder = "Search..."
    st.font = rmq.font.font_with_name('Roboto-Regular', 16)
  end  
  
  def search_input_bg(st)
    st.top = 50
    st.left = 0
    st.width = Device.screen.width
    st.height = 50    
    st.background_color = Color.white    
  end  
  
  def search_result_table(st)
    st.top = 101
    st.left = 0
    st.width = Device.screen.width
    st.height = Device.screen.height - 162
  end  
  
end