class ProfileStylesheet < PostStylesheet

  def profile_bg(st)
    st.background_color = Color.profile_bg
    st.width = Device.screen.width
    st.height = Device.screen.height - 125
    st.top = 0
    st.left = 0
  end    
  
  def profile_container_img(st)
    st.width = 50
    st.height = 50
    st.top = 25
    st.left = 25
    st.masks_to_bounds = true
    st.corner_radius = 25
  end  

  def profile_container_img_border(st)
    profile_container st
    st.width = 52
    st.height = 52
    st.top = 24
    st.left = 24
    st.masks_to_bounds = true
    st.corner_radius = 27
    st.background_color = Color.border_color2
  end    
  
  def user_profile_name(st)
    st.top = 15
    st.left = 85
    st.width = (Device.screen.width * 0.6)
    st.height = 40    
    st.color = Color.orange_title
    st.font = rmq.font.font_with_name('Roboto-Regular', 17)
    st.text_alignment = NSTextAlignmentLeft
  end  
  
  def profile_username(st)
    st.top = 40
    st.left = 85
    st.width = (Device.screen.width * 0.6)
    st.height = 40    
    st.color = Color.username_blue
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.text_alignment = NSTextAlignmentLeft
  end    

  def followers_label(st)
    follow_label st
    st.top = 16
  end
  
  def following_label(st)
    follow_label st    
    st.top = 46
  end
  
  def followers(st)
    follow_num_label st
    st.top = 16  
  end
  
  def following(st)
    follow_num_label st
    st.top = 46    
  end
  
  def follow_line(st)
    st.width = 110
    st.left = follow_left(st.width)
    st.top = 51
    st.height = 1
    st.background_color = Color.username_blue
  end         
  
  def follow_num_label(st)
    st.color = Color.orange_title
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.height = 40
    st.width = 100    
    st.text_alignment = NSTextAlignmentRight
    st.left = follow_left_num(st.width)      
  end  
  
  def follow_label(st)
    st.width = 100
    st.height = 40
    st.left = follow_left(st.width)    
    st.color = Color.follow_color       
    st.font = rmq.font.font_with_name('Roboto-Black', 12)   
    st.text_alignment = NSTextAlignmentRight
  end   

  def follow_left(width)
    (Device.screen.width - (width)) - 25
  end  
  
  def follow_left_num(width)
    (Device.screen.width - (width)) - 100
  end    
  
  def activity(st)
    default_post_activity_style st
    st.left = (Device.screen.width / 2)
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter
    st.font = rmq.font.font_with_name('Roboto-Regular', 10)      
  end
  
  def my_posts(st)
    default_post_activity_style st
    st.left = 25
    st.view.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter
    st.font = rmq.font.font_with_name('Roboto-regular', 10)      
  end
  
  def my_posts_active(st)
    default_post_activity_style st
    st.left = 25
    st.text_alignment = NSTextAlignmentCenter
    st.font = rmq.font.font_with_name('Roboto-Black', 10)      
  end  
  
  def activity_active(st)
    default_post_activity_style st
    st.left = (Device.screen.width / 2)
    st.text_alignment = NSTextAlignmentCenter
    st.font = rmq.font.font_with_name('Roboto-Black', 10)      
  end      
  
  def default_post_activity_style(st)
    st.width = (Device.screen.width - 50) / 2
    st.height = 30
    st.color = Color.username_blue   
    st.top = 100
  end  
  
  def orange_line(st)
    st.width = (Device.screen.width - 50)
    st.left = 25
    st.top = 125
    st.height = 1
    st.background_color = Color.clear_orange
  end  
  
  def post_active_line(st)
    active_line st    
    st.left = 25
  end  
  
  def activity_active_line(st)
    active_line st
    st.left = (Device.screen.width / 2)
  end
  
  def active_line(st)
    st.width = (Device.screen.width - 50) / 2
    st.top = 125
    st.height = 3
    st.background_color = Color.orange_title
  end
  
  def my_posts_table(st)
    st.top = 135
    st.left = 25
    st.width = (Device.screen.width - 50)
    st.height = Device.screen.height - 230
  end  
  
  def my_activity_table(st)
    st.top = 135
    st.left = 25
    st.width = (Device.screen.width - 50)
    st.height = Device.screen.height - 230
  end    
  
  def followers_label_public(st)
    follow_label_public st
    st.top = 31
  end
  
  def following_label_public(st)
    follow_label_public st    
    st.top = 48
  end
  
  def followers_public(st)
    follow_num_label_public st
    st.top = 31  
  end
  
  def following_public(st)
    follow_num_label_public st
    st.top = 48    
  end
  
  def follow_line_public(st)
    st.width = 105
    st.left = follow_left(st.width)
    st.top = 59
    st.height = 1
    st.background_color = Color.username_blue
  end     
  
  def follow_label_public(st)
    st.width = 100
    st.height = 40
    st.left = follow_left(st.width)    
    st.color = Color.follow_color       
    st.font = rmq.font.font_with_name('Roboto-Black', 10)   
    st.text_alignment = NSTextAlignmentRight
  end    
  
  def follow_num_label_public(st)
    st.color = Color.orange_title
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
    st.height = 40
    st.width = 100    
    st.text_alignment = NSTextAlignmentRight
    st.left = follow_left_num(st.width)      
  end   
  
  def follow_btn(st)
    st.width = 105
    st.left = follow_left(st.width)
    st.height = 20
    st.top = 20
    st.color = Color.white
    st.background_color = Color.follow_btn_color
    st.text = "+FOLLOW"
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end  
  
  def unfollow_btn(st)
    st.width = 105
    st.left = follow_left(st.width)
    st.height = 20
    st.top = 20
    st.color = Color.white
    st.background_color = Color.username_blue
    st.text = "FOLLOWING"  
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end  
end