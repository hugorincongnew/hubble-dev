class BaseStylesheetIphone5 < BaseStylesheet
  
  def logo(st)
    st.width = logo_size_w
    st.height = logo_size_h
    st.left = logo_left   
    st.top = 70
    st.image = image.resource("logo")
  end
  
  def new_account_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
  end  
  
  def create_account_line(st)
    st.width = 142
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end   
  
  def back_btn(st)
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end    
  
  def back_to_login_line(st)
    st.width = 156
    st.height = 1
    st.centered = :horizontal
    st.background_color = Color.main_grey
  end     

  def forgot_password_title(st)
    st.left = 0
    st.width = Device.screen.width
    st.height = 40
    st.color = Color.primary_color
    st.font = rmq.font.font_with_name('Roboto-Regular', 14)
    st.text = "FORGOT YOUR PASSWORD?"
    st.text_alignment = NSTextAlignmentCenter
  end   
  
  def forgot_btn(st)
    st.width = 90
    st.height = 40
    st.color = Color.main_grey
    st.left = 215
    st.font = rmq.font.font_with_name('Roboto-Regular', 12)
  end  
  
end