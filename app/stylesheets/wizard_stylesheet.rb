class WizardStylesheet < RubyMotionQuery::Stylesheet
  include ScreenSharedMethods
  
  def wizard_img_1(st)
    wizard_img st
    st.image = image.resource("icon_camera")
  end  
  
  def wizard_img_2(st)
    wizard_img st
    st.image = image.resource("icon_photo")
  end
  
  def wizard_img_3(st)
    wizard_img st
    st.image = image.resource("icon_comment")
  end    
  
  def dots_1(st)
    dots_img st
    st.image = image.resource("3dots_1")
  end  
  
  def dots_2(st)
    dots_img st
    st.image = image.resource("3dots_2")
  end
  
  def dots_3(st)
    dots_img st
    st.image = image.resource("3dots_3")
  end    
  
  def wizard_img(st)
    st.left = (Device.screen.width / 2) - (image_width / 2)
    st.top = 110
    st.width = image_width
    st.height = image_width
  end  
  
  def dots_img(st)
    st.left = (Device.screen.width / 2) - (dots_image_width / 2)
    st.top = Device.screen.height - 100
    st.width = dots_image_width
    st.height = dots_image_height
  end  
  
  def image_width
    Device.screen.width * 0.5
  end  
  
  def dots_image_width
    Device.screen.width * 0.12
  end    
  
  def dots_image_height
    (dots_image_width * 40) / 104
  end     
  
  def title_line1(st)
    title st
    st.top = 130 + image_width
  end
  
  def title_line2(st)
    title st
    st.top = 170 + image_width
    st.height = 50
    st.font = rmq.font.font_with_name('Roboto-bold', font_size_for('title2'))
  end  
  
  def text_line1(st)
    subtitle st
    st.top = 220 + image_width
  end

  def text_line2(st)
    subtitle st
    st.top = 250 + image_width    
  end
  
  def subtitle(st)
    st.width = Device.screen.width
    st.height = 40
    st.left = 0
    st.color = Color.white
    st.font = rmq.font.font_with_name('Roboto-Regular', font_size_for('subtitle'))
    st.text_alignment = NSTextAlignmentCenter
  end  
  
  def title(st)
    st.width = Device.screen.width
    st.height = 40
    st.left = 0
    st.color = Color.wizard_orange
    st.font = rmq.font.font_with_name('Roboto-Bold', 30)
    st.text_alignment = NSTextAlignmentCenter
  end    
  
  def font_size_for(element)
    WizardHelper.send("font_size_#{element}")
  end  
  
end