class WizardHelper
  def self.iphone_model
    case Device.screen.height
      when 480
        "Iphone4"  
      when 568
        "Iphone5"
      when 667
        "Iphone6"     
      when 736
        "Iphone6plus"
      else
        ""
    end        
  end  
  
  def self.font_size_title2
    case self.iphone_model
      when "Iphone4" 
        40
      when "Iphone5"
        44
      when "Iphone6"
        52
      when "Iphone6plus"
        54        
      else
        40
    end  
  end  
  
  def self.font_size_subtitle
    case self.iphone_model
      when "Iphone4" 
        14
      when "Iphone5"
        16
      else
        18
    end  
  end   
end  