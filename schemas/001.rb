schema "001" do

  entity "User" do
    integer32 :id,           optional: false    
    string    :email        
    string    :username     
    string    :full_name      
    string    :avatar
    has_many  :posts   
    has_many  :comments    
    integer32 :following
    integer32 :followers   
    string    :follow
  end
  
  entity "CurrentUser" do
    integer32 :id,           optional: false
    string    :email,        optional: false
    string    :auth_key,     optional: false
    string    :username,     optional: false
    string    :full_name,    optional: false
    string    :avatar    
    integer32 :following
    integer32 :followers     
  end
  
  entity "Post" do
    integer32   :id,           optional: false
    integer32   :user_id,      optional: false
    integer32   :likes,        optional: false
    integer32   :views,        optional: false
    string      :title
    string      :location
    string      :file
    integer32   :file_type
    string      :like
    
    has_many  :comments
    belongs_to :user
  end
  
  entity "FeedPost" do
    integer32   :id,           optional: false
    integer32   :user_id,      optional: false
    integer32   :likes,        optional: false
    integer32   :views,        optional: false
    string      :title
    string      :location
    string      :file
    integer32   :file_type
    string      :like
    
    has_many  :comments
    belongs_to :user    
  end  
  
  entity "Trending" do
    integer32   :id,           optional: false
    integer32   :user_id,      optional: false
    integer32   :likes,        optional: false
    integer32   :views,        optional: false
    string      :title
    string      :location
    string      :file
    integer32   :file_type
    string      :like
    
    has_many  :comments
    belongs_to :user
  end  
  
  entity "MostView" do
    integer32   :id,           optional: false
    integer32   :user_id,      optional: false
    integer32   :likes,        optional: false
    integer32   :views,        optional: false
    string      :title
    string      :location
    string      :file
    integer32   :file_type
    string      :like
    
    has_many  :comments
    belongs_to :user
  end  
  
  entity "MostLike" do
    integer32   :id,           optional: false
    integer32   :user_id,      optional: false
    integer32   :likes,        optional: false
    integer32   :views,        optional: false
    string      :title
    string      :location
    string      :file
    integer32   :file_type
    string      :like
    
    has_many  :comments
    belongs_to :user
  end   
  
  entity "Comment" do
    integer32   :id,           optional: false
    string    :message,        optional: false

    belongs_to :feed_post  
    belongs_to :post    
    belongs_to :user    
    belongs_to :trending
    belongs_to :most_view
    belongs_to :most_like
  end
  
  entity "Feed" do
    integer32   :id,           optional: false
    integer32   :post_id,      optional: false 
    integer32   :comment_id
    integer32   :like_id
    integer32   :user_id    
    string      :feed_type
  end  
    

end